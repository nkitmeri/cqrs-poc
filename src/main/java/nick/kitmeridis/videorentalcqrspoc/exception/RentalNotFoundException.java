package nick.kitmeridis.videorentalcqrspoc.exception;

public class RentalNotFoundException extends NotFoundException {

    public RentalNotFoundException(String message) {
        super(message);
    }

    public RentalNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
