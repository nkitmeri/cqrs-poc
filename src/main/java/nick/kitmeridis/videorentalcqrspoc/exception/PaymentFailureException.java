package nick.kitmeridis.videorentalcqrspoc.exception;

public class PaymentFailureException extends BadRequestException {

    public PaymentFailureException(String message) {
        super(message);
    }

    public PaymentFailureException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
