package nick.kitmeridis.videorentalcqrspoc.exception;

import org.springframework.http.HttpStatus;

public class InternalServerErrorException extends ServiceException {

    public InternalServerErrorException(final String message) {
        super(HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
    }

    public InternalServerErrorException(final String message, final Throwable throwable) {
        super(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, throwable);
    }
}
