package nick.kitmeridis.videorentalcqrspoc.exception;

public class BatchRentalNotFoundException extends NotFoundException {

    public BatchRentalNotFoundException(final String message) {
        super(message);
    }

    public BatchRentalNotFoundException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
