package nick.kitmeridis.videorentalcqrspoc.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ServiceException {

    public NotFoundException(final String message) {
        super(HttpStatus.NOT_FOUND.value(), message);
    }

    public NotFoundException(final String message, final Throwable throwable) {
        super(HttpStatus.NOT_FOUND.value(), message, throwable);
    }
}
