package nick.kitmeridis.videorentalcqrspoc.exception;

public class FilmNotFoundException extends NotFoundException {

    public FilmNotFoundException(final String message) {
        super(message);
    }

    public FilmNotFoundException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}

