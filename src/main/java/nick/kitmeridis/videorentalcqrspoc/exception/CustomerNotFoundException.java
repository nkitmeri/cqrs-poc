package nick.kitmeridis.videorentalcqrspoc.exception;

public class CustomerNotFoundException extends NotFoundException {

    public CustomerNotFoundException(final String message) {
        super(message);
    }

    public CustomerNotFoundException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}