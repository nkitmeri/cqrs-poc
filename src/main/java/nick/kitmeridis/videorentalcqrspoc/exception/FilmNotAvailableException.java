package nick.kitmeridis.videorentalcqrspoc.exception;

public class FilmNotAvailableException extends BadRequestException {

    public FilmNotAvailableException(String message) {
        super(message);
    }

    public FilmNotAvailableException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
