package nick.kitmeridis.videorentalcqrspoc.exception;

import lombok.Getter;

public class ServiceException extends Exception {

    @Getter
    private final int statusCode;

    public ServiceException(final int statusCode, final String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public ServiceException(final int statusCode, final String message, final Throwable throwable) {
        super(message, throwable);
        this.statusCode = statusCode;
    }
}
