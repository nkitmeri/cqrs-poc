package nick.kitmeridis.videorentalcqrspoc.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends ServiceException {

    public BadRequestException(String message) {
        super(HttpStatus.BAD_REQUEST.value(), message);
    }

    public BadRequestException(String message, Throwable throwable) {
        super(HttpStatus.BAD_REQUEST.value(), message, throwable);
    }
}
