package nick.kitmeridis.videorentalcqrspoc.controller;

import ma.glasnost.orika.MapperFactory;

/**
 * Interface that accepts a mapperfactory and configures specific mappings.
 */
public interface MapperConfigurer {

    void configure(final MapperFactory mapperFactory);
}
