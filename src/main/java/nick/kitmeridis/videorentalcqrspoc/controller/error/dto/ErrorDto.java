package nick.kitmeridis.videorentalcqrspoc.controller.error.dto;

import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
public class ErrorDto {

    private String message;

    private HttpStatus httpStatus;
}
