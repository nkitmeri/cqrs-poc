package nick.kitmeridis.videorentalcqrspoc.controller.listing.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "ReturnedRentalDetailsDto",
          description = "Represents information related to a returned batch rental operation.")
@Getter
@Setter
public class ReturnedRentalDetailsDto extends BaseRentalDetailsDto {

    @ApiModelProperty(value = "The extra cost due to delayed return of films of the rental operation",
            readOnly = true)
    private Double extraCost;
}
