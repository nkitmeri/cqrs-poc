package nick.kitmeridis.videorentalcqrspoc.controller.listing;

import ma.glasnost.orika.MapperFacade;
import nick.kitmeridis.videorentalcqrspoc.controller.listing.dto.RentalDetailsDto;
import nick.kitmeridis.videorentalcqrspoc.controller.listing.dto.ReturnedRentalDetailsDto;
import nick.kitmeridis.videorentalcqrspoc.queryside.service.RentalDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequestMapping("/rental-details")
public class RentalDetailsControllerImpl implements RentalDetailsController {

    private final RentalDetailsService rentalDetailsService;
    private final MapperFacade mapperFacade;

    public RentalDetailsControllerImpl(final RentalDetailsService rentalDetailsService,
                                       final MapperFacade mapperFacade) {

        this.rentalDetailsService = rentalDetailsService;
        this.mapperFacade = mapperFacade;
    }

    @GetMapping("{batchId}")
    public Callable<RentalDetailsDto> getRentalDetails(@PathVariable("batchId") final String batchId) {

        return () -> this.mapperFacade.map(rentalDetailsService.getRentalDetails(batchId), RentalDetailsDto.class);
    }

    @GetMapping("return/{batchId}")
    public Callable<ReturnedRentalDetailsDto> getReturnedRentalDetails(@PathVariable("batchId") final String batchId) {

        return () -> this.mapperFacade.map(
                rentalDetailsService.getReturnedRentalDetails(batchId), ReturnedRentalDetailsDto.class);
    }
}
