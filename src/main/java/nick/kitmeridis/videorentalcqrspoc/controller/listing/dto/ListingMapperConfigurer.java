package nick.kitmeridis.videorentalcqrspoc.controller.listing.dto;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import nick.kitmeridis.videorentalcqrspoc.controller.MapperConfigurer;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.FilmInventoryItem;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalDetails;


public class ListingMapperConfigurer implements MapperConfigurer {

    @Override
    public void configure(final MapperFactory mapperFactory) {

        mapperFactory
                .classMap(FilmInventoryItem.class, FilmInventoryItemDto.class)
                .customize(new CustomMapper<FilmInventoryItem, FilmInventoryItemDto>() {

                    @Override
                    public void mapAtoB(final FilmInventoryItem filmInventoryItem,
                                        final FilmInventoryItemDto filmInventoryItemDto,
                                        final MappingContext context) {

                       final long availableCopies = filmInventoryItem.getAvailableCopies();

                       filmInventoryItemDto.setAvailable(availableCopies > 0);
                    }
                })
                .byDefault()
                .register();

        mapperFactory
                .classMap(RentalDetails.class, BaseRentalDetailsDto.class)
                .fieldAToB("returnedDay", "returnDate")
                .byDefault()
                .register();

        mapperFactory
                .classMap(RentalDetails.class, RentalDetailsDto.class)
                .use(RentalDetails.class, BaseRentalDetailsDto.class)
                .byDefault()
                .register();

        mapperFactory
                .classMap(RentalDetails.class, ReturnedRentalDetailsDto.class)
                .fieldAToB("totalCost", "extraCost")
                .use(RentalDetails.class, BaseRentalDetailsDto.class)
                .register();

        mapperFactory
                .classMap(RentalDetails.RentedFilm.class, RentedFilmDto.class)
                .byDefault()
                .register();
    }
}
