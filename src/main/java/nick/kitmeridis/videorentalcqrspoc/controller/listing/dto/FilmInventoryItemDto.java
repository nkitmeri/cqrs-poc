package nick.kitmeridis.videorentalcqrspoc.controller.listing.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "FilmInventoryItemDto",
          description = "Represents information related to a film regarding its name, availability etc.")
@Getter
@Setter
public class FilmInventoryItemDto {

    private long filmId;

    private String name;

    private long baseChargingDays;

    private long baseChargingPrice;

    private boolean isAvailable;
}
