package nick.kitmeridis.videorentalcqrspoc.controller.listing;

import ma.glasnost.orika.MapperFacade;
import nick.kitmeridis.videorentalcqrspoc.controller.listing.dto.FilmInventoryItemDto;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.FilmInventoryItem;
import nick.kitmeridis.videorentalcqrspoc.queryside.service.FilmInventoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequestMapping("/films")
public class InventoryControllerImpl implements InventoryController {


    private final FilmInventoryService filmInventoryService;

    private final MapperFacade mapperFacade;

    public InventoryControllerImpl(final FilmInventoryService filmInventoryService,
                                   final MapperFacade mapperFacade) {

        this.filmInventoryService = filmInventoryService;
        this.mapperFacade = mapperFacade;
    }

    @GetMapping("inventory")
    public Callable<Page<FilmInventoryItemDto>> getInventory(final Pageable pageable) {

        return () -> {
            final Page<FilmInventoryItem> inventoryItems = filmInventoryService.getFilmInventory(pageable);

            return inventoryItems.map(item -> mapperFacade.map(item, FilmInventoryItemDto.class));
        };
    }
}
