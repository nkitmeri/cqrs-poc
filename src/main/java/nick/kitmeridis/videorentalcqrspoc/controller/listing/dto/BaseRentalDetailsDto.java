package nick.kitmeridis.videorentalcqrspoc.controller.listing.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class BaseRentalDetailsDto {

    @ApiModelProperty(value = "The batch id of the operation", readOnly = true)
    protected String batchId;

    @ApiModelProperty(value = "A collections that wraps information about the name and the cost of each film of the " +
            "batch rental operation", readOnly = true)
    protected Collection<RentedFilmDto> rentedFilms;

    @ApiModelProperty(value = "The declared return date which is used to calculated the price the customer will pay " +
            "before the rental operation take place.", readOnly = true)
    protected String returnDate;

}
