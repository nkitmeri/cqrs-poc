package nick.kitmeridis.videorentalcqrspoc.controller.listing.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "RentalDetailsDto",
         description = "Represents information related to a batch rental operation.")
@Getter
@Setter
public class RentalDetailsDto extends BaseRentalDetailsDto {

    @ApiModelProperty(value = "The total calculated price that sums up the cost of for all film of the rental operation",
                      readOnly = true)
    private Double totalCost;
}
