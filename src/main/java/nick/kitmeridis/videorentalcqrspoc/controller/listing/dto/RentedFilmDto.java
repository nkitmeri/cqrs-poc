package nick.kitmeridis.videorentalcqrspoc.controller.listing.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "RentedFilmDto",
        description = "Represents information related to a rented film regarding its name, and calculated price.")
@Getter
@Setter
public class RentedFilmDto {

    @ApiModelProperty(value = "The name of the film", readOnly = true)
    private String filmName;

    @ApiModelProperty(value = "The calculated price of the film", readOnly = true)
    private Double filmPrice;
}
