package nick.kitmeridis.videorentalcqrspoc.controller.listing.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "FilmDetailsDto",
          description = "Represents information related to a film regarding its name, availability etc.")
@Getter
@Setter
public class FilmDetailsDto {

    @ApiModelProperty(value = "The identification of the film", readOnly = true)
    private Long id;

    @ApiModelProperty(value = "The name of the film", readOnly = true)
    private String name;

    @ApiModelProperty(value = "The number of available copies", readOnly = true)
    private long availableCopies;

    @ApiModelProperty(value = "The price for the film rental for each charging time unit", readOnly = true)
    private long basePrice;

    @ApiModelProperty(value = "The film's charging time unit, i.e. the maximum days before daily charging start.",
                      readOnly = true)
    private long baseChargingDays;

    @ApiModelProperty(
            value = "A message indicating if the film is available now or when it will be probably available or even " +
                    "whether it its return is delayed.",
            readOnly = true)
    private String possibleAvailabilityDateMessage;

}
