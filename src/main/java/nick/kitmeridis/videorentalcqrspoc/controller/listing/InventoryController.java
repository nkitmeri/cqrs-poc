package nick.kitmeridis.videorentalcqrspoc.controller.listing;

import nick.kitmeridis.videorentalcqrspoc.controller.listing.dto.FilmInventoryItemDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.concurrent.Callable;

public interface InventoryController {

    Callable<Page<FilmInventoryItemDto>> getInventory(final Pageable pageable);
}
