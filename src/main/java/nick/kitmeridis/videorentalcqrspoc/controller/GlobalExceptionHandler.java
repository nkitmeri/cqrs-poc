package nick.kitmeridis.videorentalcqrspoc.controller;

import lombok.extern.slf4j.Slf4j;
import nick.kitmeridis.videorentalcqrspoc.controller.error.dto.ErrorDto;
import nick.kitmeridis.videorentalcqrspoc.exception.BadRequestException;
import nick.kitmeridis.videorentalcqrspoc.exception.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.Collection;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ResponseEntity<Collection<ErrorDto>> handleConstraintViolationExceptions(
                                                                         final HttpServletRequest request,
                                                                         final ConstraintViolationException exception) {

        log.error(exception.getMessage(), exception);

        final Collection<ErrorDto> errors = exception.getConstraintViolations().stream()
                .map(constraintViolation -> {
                    final String message = new StringJoiner(": ")
                            .add(constraintViolation.getPropertyPath().toString())
                            .add(constraintViolation.getMessage())
                            .toString();

                    return new ErrorDto(message, HttpStatus.BAD_REQUEST);
                })
                .collect(Collectors.toList());

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(errors, headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorDto> handleNotFoundExceptions(final HttpServletRequest request,
                                                             final Exception exception) {

        log.error(exception.getMessage(), exception);

        final ErrorDto error = new ErrorDto(exception.getMessage(), HttpStatus.NOT_FOUND);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(error, headers, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<ErrorDto> handleBadRequestExceptions(final HttpServletRequest request,
                                                             final Exception exception) {

        log.error(exception.getMessage(), exception);

        final ErrorDto error = new ErrorDto(exception.getMessage(), HttpStatus.BAD_REQUEST);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(error, headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorDto> handleGenericExceptions(final HttpServletRequest request,
                                                            final Exception exception) {

        log.error(exception.getMessage(), exception);

        final String message = "An error occurred. Please try again.";
        final ErrorDto error = new ErrorDto(message, HttpStatus.INTERNAL_SERVER_ERROR);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(error, headers, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

