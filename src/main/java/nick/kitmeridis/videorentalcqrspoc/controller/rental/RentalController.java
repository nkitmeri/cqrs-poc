package nick.kitmeridis.videorentalcqrspoc.controller.rental;

import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.RentalDto;
import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.ReturnRentalDto;

import java.util.concurrent.Callable;

public interface RentalController {

    Callable<String> rent(final RentalDto rentalDto);

    Callable<String> returnRental(final ReturnRentalDto returnRentalDto);
}
