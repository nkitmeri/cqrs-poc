package nick.kitmeridis.videorentalcqrspoc.controller.rental;

import ma.glasnost.orika.MapperFacade;
import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.RentCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.ReturnRentalCommand;
import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.RentalDto;
import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.ReturnRentalDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequestMapping("/rental")
public class RentalControllerImpl implements RentalController {

    private final MapperFacade mapperFacade;

    private final CommandHandler<RentCommand> rentCommandHandler;

    private final CommandHandler<ReturnRentalCommand> returnRentalCommandHandler;

    public RentalControllerImpl(final MapperFacade mapperFacade,
                                @Qualifier("rentFilmCommandHandler")
                                final CommandHandler<RentCommand> rentCommandHandler,
                                @Qualifier("returnRentalCommandHandler")
                                final CommandHandler<ReturnRentalCommand> returnRentalCommandHandler) {

        this.mapperFacade = mapperFacade;
        this.rentCommandHandler = rentCommandHandler;
        this.returnRentalCommandHandler = returnRentalCommandHandler;
    }

    @PostMapping
    public Callable<String> rent(@RequestBody final RentalDto rentalDto) {

        final RentCommand rentCommand = mapperFacade.map(rentalDto, RentCommand.class);

        return () -> rentCommandHandler.apply(rentCommand);
    }

    @PostMapping("/return")
    public Callable<String> returnRental(@RequestBody final ReturnRentalDto returnRentalDto) {

        final ReturnRentalCommand returnRentalCommand = mapperFacade.map(returnRentalDto, ReturnRentalCommand.class);

        return () -> returnRentalCommandHandler.apply(returnRentalCommand);
    }
}
