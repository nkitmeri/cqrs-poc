package nick.kitmeridis.videorentalcqrspoc.controller.rental.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Collection;

@ApiModel(value = "RentalDto",
          description = "Represents information regarding the rental action of one or multiple films")
@Getter
@Setter
public class RentalDto {

    @ApiModelProperty(value = "The identification of the customer.", required = true)
    private Long customerId;

    @ApiModelProperty(value = "The identifications of the films to be rented.", required = true)
    private Collection<Long> filmIds;

    @ApiModelProperty(value = "The declared returned date.", required = true)
    private LocalDate returnDate;
}
