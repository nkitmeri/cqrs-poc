package nick.kitmeridis.videorentalcqrspoc.controller.rental.dto;

import ma.glasnost.orika.MapperFactory;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.PaymentCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.RentCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.ReturnRentalCommand;
import nick.kitmeridis.videorentalcqrspoc.controller.MapperConfigurer;

public class RentalMapperConfigurer implements MapperConfigurer {

    @Override
    public void configure(MapperFactory mapperFactory) {

        mapperFactory.classMap(RentCommand.class, RentalDto.class).byDefault().register();

        mapperFactory.classMap(PaymentCommand.class, PaymentDto.class).byDefault().register();

        mapperFactory.classMap(ReturnRentalCommand.class, ReturnRentalDto.class).byDefault().register();
    }
}
