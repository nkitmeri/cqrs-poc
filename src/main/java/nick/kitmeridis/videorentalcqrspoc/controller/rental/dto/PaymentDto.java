package nick.kitmeridis.videorentalcqrspoc.controller.rental.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(value = "PaymentDto",
        description = "Represents information regarding the front payment action for a batch rental operation")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDto {

    @ApiModelProperty(value = "The identification of the customer.", required = true)
    private Long customerId;

    @ApiModelProperty(value = "The batch id of the rental operation for which the payment takes place.", required = true)
    private String batchId;

    @ApiModelProperty(value = "The payment method eg. Paypal, Credit Card etc.", required = true)
    private String paymentMethod;

    @ApiModelProperty(value = "The customers card number.", required = true)
    private String cardNumber;

    @ApiModelProperty(value = "The total amount of the rental operation that the customer pays.", required = true)
    private double amount;
}