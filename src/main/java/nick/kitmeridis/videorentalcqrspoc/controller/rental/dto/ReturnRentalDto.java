package nick.kitmeridis.videorentalcqrspoc.controller.rental.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(value = "ReturnRentalDto",
        description = "Represents information regarding the return a batch rental operation," +
                      " eg. if customer needs to pay extra amount due to delay etc.")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReturnRentalDto {

    @ApiModelProperty(value = "The batch id of the rental operation for which the payment takes place.", required = true)
    private String batchId;

    @ApiModelProperty(value = "The payment method eg. Paypal, Credit Card etc.", required = true)
    private String paymentMethod;

    @ApiModelProperty(value = "The customers card number.", required = true)
    private String cardNumber;

    @ApiModelProperty(value = "The possible extra amount due to delay or 0 in case of no delay.", required = true)
    private double extraAmount;
}
