package nick.kitmeridis.videorentalcqrspoc.controller.rental;

import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.PaymentDto;

import java.util.concurrent.Callable;

public interface PaymentController {

    Callable<String> pay(final PaymentDto paymentDto);
}
