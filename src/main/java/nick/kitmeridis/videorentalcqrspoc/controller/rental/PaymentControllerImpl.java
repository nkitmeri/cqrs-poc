package nick.kitmeridis.videorentalcqrspoc.controller.rental;

import ma.glasnost.orika.MapperFacade;
import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.PaymentCommand;
import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.PaymentDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequestMapping("/payment")
public class PaymentControllerImpl implements PaymentController {

    private final MapperFacade mapperFacade;

    private final CommandHandler<PaymentCommand> commandHandler;

    public PaymentControllerImpl(final MapperFacade mapperFacade,
                                @Qualifier("paymentCommandHandler")
                                final CommandHandler<PaymentCommand> commandHandler) {

        this.mapperFacade = mapperFacade;
        this.commandHandler = commandHandler;
    }

    @Override
    @PostMapping
    public Callable<String> pay(@RequestBody final PaymentDto paymentDto) {

        final PaymentCommand command = mapperFacade.map(paymentDto, PaymentCommand.class);

        return () -> commandHandler.apply(command);
    }
}
