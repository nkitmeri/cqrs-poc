package nick.kitmeridis.videorentalcqrspoc.configuration;

import com.google.common.collect.ImmutableList;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import nick.kitmeridis.videorentalcqrspoc.controller.listing.dto.ListingMapperConfigurer;
import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.RentalMapperConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

/**
 * Spring configuration for orika mapper facade.
 */
@Configuration
public class MapperConfiguration {

    @Bean
    public MapperFacade mapperFacade() {

        final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.getConverterFactory()
                     .registerConverter(new PassThroughConverter(LocalDate.class));

        ImmutableList.of(
            new ListingMapperConfigurer(),
            new RentalMapperConfigurer()

        ).forEach(configurer -> configurer.configure(mapperFactory));

        return mapperFactory.getMapperFacade();
    }
}
