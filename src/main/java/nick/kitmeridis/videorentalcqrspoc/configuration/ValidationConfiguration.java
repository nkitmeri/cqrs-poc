package nick.kitmeridis.videorentalcqrspoc.configuration;

import nick.kitmeridis.videorentalcqrspoc.infrastructure.ValidationUtil;
import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.validation.Validator;

@Configuration
public class ValidationConfiguration {

    @Bean
    public LocalValidatorFactoryBean validator(){

        final LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setProviderClass(HibernateValidator.class);

        return validator;
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor(final Validator validator) {

        final MethodValidationPostProcessor methodValidationPostProcessor = new MethodValidationPostProcessor();
        methodValidationPostProcessor.setValidator(validator);

        return methodValidationPostProcessor;
    }

    @Bean
    public ValidationUtil validationUtil(final Validator validator) {
        return new ValidationUtil(validator);
    }
}
