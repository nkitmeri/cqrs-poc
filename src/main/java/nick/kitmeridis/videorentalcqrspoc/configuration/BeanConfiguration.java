package nick.kitmeridis.videorentalcqrspoc.configuration;

import com.google.common.collect.ImmutableMap;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.handler.PaymentCommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.handler.RentCommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.handler.ReturnRentalCommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.customer.CustomerRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.film.FilmRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental.RentalRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BatchIdGeneratorService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BatchIdGeneratorServiceImpl;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BonusPointsService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BonusPointsServiceImpl;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.PricingIntegrationService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.PricingIntegrationServiceImpl;
import nick.kitmeridis.videorentalcqrspoc.infrastructure.ValidationUtil;
import nick.kitmeridis.videorentalcqrspoc.queryside.repository.FilmInventoryItemRepository;
import nick.kitmeridis.videorentalcqrspoc.queryside.repository.RentalViewItemRepository;
import nick.kitmeridis.videorentalcqrspoc.queryside.service.FilmInventoryService;
import nick.kitmeridis.videorentalcqrspoc.queryside.service.FilmInventoryServiceImpl;
import nick.kitmeridis.videorentalcqrspoc.queryside.service.RentalDetailsService;
import nick.kitmeridis.videorentalcqrspoc.queryside.service.RentalDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;

import java.util.Map;

@Configuration
public class BeanConfiguration {

    @Value("${pricing.gateway.url}")
    private String pricingGatewayUrl;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    @Setter
    class BonusPointsProperties {
        private Long newFilm;
        private Long regularFilm;
        private Long oldFilm;
    }

    @Bean
    @ConfigurationProperties(prefix="bonus")
    public BonusPointsProperties bonusPointsMapProperties() {
        return new BonusPointsProperties();
    }

    @Bean
    public BatchIdGeneratorService batchIdGeneratorService() {
        return new BatchIdGeneratorServiceImpl();
    }

    @Bean
    public PricingIntegrationService pricingIntegrationService(final RestOperations restOperations) {
        return new PricingIntegrationServiceImpl(this.pricingGatewayUrl, restOperations);
    }

    @Bean
    @ConfigurationProperties(value = "bonus")
    public BonusPointsService bonusPointsService(final BonusPointsProperties bonusPointsProperties) {

        final Map<String, Long> bonusPointsMap = ImmutableMap.of("new", bonusPointsProperties.getNewFilm(),
                                                                "regular", bonusPointsProperties.getRegularFilm(),
                                                                "old", bonusPointsProperties.getOldFilm());
        return new BonusPointsServiceImpl(bonusPointsMap);
    }

    @Bean(name = "rentFilmCommandHandler")
    public CommandHandler rentFilmCommandHandler(final BatchIdGeneratorService batchIdGeneratorService,
                                                 final CustomerRepository customerRepository,
                                                 final FilmRepository filmRepository,
                                                 final RentalRepository rentalRepository,
                                                 final ValidationUtil validationUtil) {

        return new RentCommandHandler(batchIdGeneratorService,
                                          customerRepository,
                                          filmRepository,
                                          rentalRepository,
                                          validationUtil);
    }

    @Bean(name = "paymentCommandHandler")
    public CommandHandler paymentCommandHandler(final PricingIntegrationService pricingIntegrationService,
                                                final BonusPointsService bonusPointsMap,
                                                final RentalRepository rentalRepository,
                                                final CustomerRepository customerRepository,
                                                final FilmRepository filmRepository,
                                                final ValidationUtil validationUtil) {

        return new PaymentCommandHandler(pricingIntegrationService,
                                         bonusPointsMap,
                                         rentalRepository,
                                         customerRepository,
                                         filmRepository,
                                         validationUtil);
    }

    @Bean(name = "returnRentalCommandHandler")
    public CommandHandler returnRentalCommandHandler(final PricingIntegrationService pricingIntegrationService,
                                                     final RentalRepository rentalRepository,
                                                     final ValidationUtil validationUtil) {
        return new ReturnRentalCommandHandler(pricingIntegrationService, rentalRepository, validationUtil);
    }

    @Bean
    public FilmInventoryService filmInventoryService(final FilmInventoryItemRepository filmInventoryItemRepository) {

        return new FilmInventoryServiceImpl(filmInventoryItemRepository);
    }

    @Bean
    public RentalDetailsService rentalDetailsService(final RentalViewItemRepository rentalViewItemRepository) {

        return new RentalDetailsServiceImpl(rentalViewItemRepository);

    }
}
