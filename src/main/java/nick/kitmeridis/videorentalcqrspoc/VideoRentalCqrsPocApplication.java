package nick.kitmeridis.videorentalcqrspoc;

import nick.kitmeridis.videorentalcqrspoc.configuration.BeanConfiguration;
import nick.kitmeridis.videorentalcqrspoc.configuration.MapperConfiguration;
import nick.kitmeridis.videorentalcqrspoc.configuration.SpringFoxConfiguration;
import nick.kitmeridis.videorentalcqrspoc.configuration.SystemConfiguration;
import nick.kitmeridis.videorentalcqrspoc.configuration.ValidationConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan(basePackages = "nick.kitmeridis.videorentalcqrspoc.controller")
@EnableSwagger2
@Import({
        SystemConfiguration.class,
        ValidationConfiguration.class,
        BeanConfiguration.class,
        SpringFoxConfiguration.class,
        MapperConfiguration.class
})
public class VideoRentalCqrsPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideoRentalCqrsPocApplication.class, args);
    }
}
