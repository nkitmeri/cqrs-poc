package nick.kitmeridis.videorentalcqrspoc.queryside.repository;

import nick.kitmeridis.videorentalcqrspoc.queryside.model.FilmInventoryItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmInventoryItemRepository extends PagingAndSortingRepository<FilmInventoryItem, Long> {
}
