package nick.kitmeridis.videorentalcqrspoc.queryside.repository;

import nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalViewItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RentalViewItemRepository extends PagingAndSortingRepository<RentalViewItem, String> {

    List<RentalViewItem> findAllByBatchId(final String batchId);
}
