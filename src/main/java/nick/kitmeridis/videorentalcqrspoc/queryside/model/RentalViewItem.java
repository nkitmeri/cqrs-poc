package nick.kitmeridis.videorentalcqrspoc.queryside.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "rental_view_item")
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RentalViewItem {

    @Id
    @NotBlank
    private String id;

    @NotBlank
    private String batchId;

    @NotBlank
    private String filmName;

    @NotNull
    private LocalDate rentalDate;

    @NotNull
    private LocalDate declaredReturnedDate;

    @NotNull
    @Min(0)
    private Long baseChargingDays;

    @NotNull
    @Min(0)
    private Long basePrice;

    public Double getPrice() {

        final long rentalDays = this.rentalDate.until(this.declaredReturnedDate, ChronoUnit.DAYS) + 1;

        if (rentalDays <= this.baseChargingDays) {
            return Double.valueOf(this.basePrice);
        } else {
            final long extraDays = rentalDays - this.baseChargingDays;

            return (double) ((extraDays + 1) * this.basePrice);
        }
    }

    public Double getDelayedPrice() {

        final long extraDays = this.declaredReturnedDate.until(LocalDate.now(), ChronoUnit.DAYS) + 1;

        return extraDays > 0 ? (double) extraDays * basePrice : 0.0;
    }
}
