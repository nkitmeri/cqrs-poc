package nick.kitmeridis.videorentalcqrspoc.queryside.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "film_inventory_item")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FilmInventoryItem {

    @Id
    @Column(name = "film_id")
    private long filmId;

    private String name;

    @Column(name = "charging_days")
    private long baseChargingDays;

    @Column(name = "base_price")
    private long baseChargingPrice;

    @Column(name = "availability")
    private long availableCopies;
}
