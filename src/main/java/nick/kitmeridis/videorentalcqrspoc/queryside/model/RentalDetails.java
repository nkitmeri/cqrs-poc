package nick.kitmeridis.videorentalcqrspoc.queryside.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collection;

@AllArgsConstructor
@Getter
public class RentalDetails {

    @NotNull
    private String batchId;

    private Collection<RentedFilm> rentedFilms;

    private LocalDate returnedDay;

    private Double totalCost;

    @AllArgsConstructor
    @Getter
    public static class RentedFilm {

        private String filmName;

        private Double filmPrice;
    }
}
