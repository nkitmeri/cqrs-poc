package nick.kitmeridis.videorentalcqrspoc.queryside.service;

import nick.kitmeridis.videorentalcqrspoc.queryside.model.FilmInventoryItem;
import nick.kitmeridis.videorentalcqrspoc.queryside.repository.FilmInventoryItemRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;

public class FilmInventoryServiceImpl implements FilmInventoryService {

    private FilmInventoryItemRepository filmInventoryItemRepository;

    public FilmInventoryServiceImpl(@NotNull final FilmInventoryItemRepository filmInventoryItemRepository) {

        this.filmInventoryItemRepository = filmInventoryItemRepository;
    }

    public Page<FilmInventoryItem> getFilmInventory(final Pageable pageable) {

        return filmInventoryItemRepository.findAll(pageable);
    }
}
