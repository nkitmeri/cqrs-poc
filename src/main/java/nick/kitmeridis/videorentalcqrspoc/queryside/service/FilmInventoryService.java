package nick.kitmeridis.videorentalcqrspoc.queryside.service;

import nick.kitmeridis.videorentalcqrspoc.queryside.model.FilmInventoryItem;
import org.hibernate.validator.valuehandling.UnwrapValidatedValue;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public interface FilmInventoryService {

    /**
     * Returns a {@link Page} instance that wraps a portion of the stored films,
     * which size is specified by the provided {@code pageable} parameter,
     * as {@link FilmInventoryItem} instances.
     * @param pageable the Pageable instance that contains information related to the page number and size
     *                 of the inventory to be retrieved.
     * @return the specified page of the film inventory.
     */
    @NotNull
    @UnwrapValidatedValue(false)
    Page<FilmInventoryItem> getFilmInventory(@NotNull final Pageable pageable);
}
