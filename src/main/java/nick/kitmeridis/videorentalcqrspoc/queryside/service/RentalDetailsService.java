package nick.kitmeridis.videorentalcqrspoc.queryside.service;

import nick.kitmeridis.videorentalcqrspoc.exception.RentalNotFoundException;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalDetails;

import javax.validation.constraints.NotNull;

public interface RentalDetailsService {

    /**
     * Returns information related to the batch rental operation of the provided id.
     * It should be used to get information before the actual action of rental takes places so as to provided
     * information related - among others - to the amount that the customer must pay front.
     * @param batchId the batch id that corresponds to the rental operation for which information is to be returned.
     * @return a {@link RentalDetails} intances containg information for the batch rental operation of hte provided
     *         {@code batchId}.
     * @throws RentalNotFoundException in case no store rental has the provided batch id.
     */
    RentalDetails getRentalDetails(@NotNull final String batchId) throws RentalNotFoundException;

    /**
     * Returns information related to the batch rental operation of the provided id.
     * It should be used to get information before the actual action of rental finish (i.e the customer returns the
     * films) so as to provided information related - among others - to the extra amount that the customer must pay due
     * to possible return delay, if any.
     * @param batchId the batch id that corresponds to the rental operation for which information is to be returned.
     * @return a {@link RentalDetails} intances containg information for the batch rental operation of hte provided
     *         {@code batchId}.
     * @throws RentalNotFoundException in case no store rental has the provided batch id.
     */
    RentalDetails getReturnedRentalDetails(@NotNull final String batchId) throws RentalNotFoundException;
}
