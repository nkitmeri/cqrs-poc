package nick.kitmeridis.videorentalcqrspoc.queryside.service;

import nick.kitmeridis.videorentalcqrspoc.exception.RentalNotFoundException;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalDetails;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalViewItem;
import nick.kitmeridis.videorentalcqrspoc.queryside.repository.RentalViewItemRepository;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

@Validated
public class RentalDetailsServiceImpl implements RentalDetailsService {

    private RentalViewItemRepository rentalViewItemRepository;

    public RentalDetailsServiceImpl(final RentalViewItemRepository rentalViewItemRepository) {

        this.rentalViewItemRepository = rentalViewItemRepository;
    }

    @Override
    public RentalDetails getRentalDetails(final String batchId) throws RentalNotFoundException {

        return this.getRentalDetails(batchId, false);
    }

    @Override
    public RentalDetails getReturnedRentalDetails(final String batchId) throws RentalNotFoundException {

        return this.getRentalDetails(batchId, true);
    }

    private RentalDetails getRentalDetails(final String batchId, final boolean isReturn) throws
            RentalNotFoundException {

        final Collection<RentalViewItem> batchRentalItems = rentalViewItemRepository.findAllByBatchId(batchId);

        final RentalViewItem firstOfBatch = batchRentalItems.stream()
                .findFirst()
                .orElseThrow(() -> new RentalNotFoundException(
                        String.format("No rental items found for batch id: %s", batchId)));

        final Collection<RentalDetails.RentedFilm> rentedFilms = batchRentalItems
                .stream()
                .map(rentalViewItem -> new RentalDetails.RentedFilm(
                                rentalViewItem.getFilmName(),
                                this.calculateFilmCost(rentalViewItem, isReturn)))
                .collect(Collectors.toList());

        final Double totalCost = rentedFilms.stream()
                .map(RentalDetails.RentedFilm::getFilmPrice)
                .reduce(0.0, (x, y) -> x + y);

        return new RentalDetails(batchId,
                                 rentedFilms,
                                 isReturn ? LocalDate.now() : firstOfBatch.getDeclaredReturnedDate(),
                                 totalCost);
    }

    private double calculateFilmCost(final RentalViewItem rentalViewItem, final boolean isReturn) {

        return isReturn ? rentalViewItem.getDelayedPrice() : rentalViewItem.getPrice();
    }
}
