package nick.kitmeridis.videorentalcqrspoc.commandside.handler;

import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.ReturnRentalCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.Rental;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalStatus;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental.RentalRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.PricingIntegrationService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.dto.PricingResponse;
import nick.kitmeridis.videorentalcqrspoc.exception.PaymentFailureException;
import nick.kitmeridis.videorentalcqrspoc.infrastructure.ValidationUtil;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link CommandHandler} for handling {@link ReturnRentalCommand}s for the scenario
 * of returning one or more films that has been rented and handling the payment of
 * possible extra charges due to delay of return.
 */
public class ReturnRentalCommandHandler implements CommandHandler<ReturnRentalCommand> {

    private final PricingIntegrationService pricingIntegrationService;

    private final RentalRepository rentalRepository;

    private final ValidationUtil validationUtil;

    public ReturnRentalCommandHandler(final PricingIntegrationService pricingIntegrationService,
                                      final RentalRepository rentalRepository,
                                      final ValidationUtil validationUtil) {

        this.pricingIntegrationService = pricingIntegrationService;
        this.rentalRepository = rentalRepository;
        this.validationUtil = validationUtil;
    }

    /**
     * This handler is responsible to proceed to the payment of any possible  extra amount due to delay and then
     * dismiss the films that  was reserved by the batch rental operation that corresponds to the provided batchId
     * of the input's {@link ReturnRentalCommand}.
     * @param command an instance of {@link ReturnRentalCommand} that contains all the needed info for payoff of
     *                any extra charging and dismissing previously rented films.
     * @return return the batch id of the batch rental operation.
     * @throws PaymentFailureException in case the pricing gateway integration request fails.
     */
    @Override
    @Transactional
    public String apply(final ReturnRentalCommand command) throws PaymentFailureException {

        validationUtil.validate(command);

        if (command.getExtraAmount() > 0) {
            this.proceedWithPayment(command);
        }

        final List<Rental> updatedRentals = rentalRepository.findAllByBatchId(command.getBatchId())
                .stream()
                .map(rental -> new Rental(rental.getId(),
                        rental.getBatchId(),
                        rental.getFilmId(),
                        rental.getCustomerId(),
                        RentalStatus.RESOLVED,
                        rental.getRentalDate(),
                        rental.getDeclaredReturnedDate(),
                        LocalDate.now()))
                .collect(Collectors.toList());

        rentalRepository.save(updatedRentals);

        return command.getBatchId();
    }

    private void proceedWithPayment(final ReturnRentalCommand command) throws PaymentFailureException {

        final PricingResponse pricingResponse = pricingIntegrationService.pay(command.getCardNumber(),
                command.getPaymentMethod(),
                command.getExtraAmount());
        // if payment fails throw exception
        if (pricingResponse.getRequestStatus() != HttpStatus.OK.value()) {
            throw new PaymentFailureException(String.format(
                    "Payment for rental with batch id %s failed", command.getBatchId()));
        }
    }
}
