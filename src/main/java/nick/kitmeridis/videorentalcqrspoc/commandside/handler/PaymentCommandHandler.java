package nick.kitmeridis.videorentalcqrspoc.commandside.handler;

import com.google.common.collect.Lists;
import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.PaymentCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.customer.Customer;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Category;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.FilmCategoryProjection;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.Rental;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalStatus;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.customer.CustomerRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.film.FilmRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental.RentalRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BonusPointsService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.PricingIntegrationService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.dto.PricingResponse;
import nick.kitmeridis.videorentalcqrspoc.exception.CustomerNotFoundException;
import nick.kitmeridis.videorentalcqrspoc.exception.PaymentFailureException;
import nick.kitmeridis.videorentalcqrspoc.infrastructure.ValidationUtil;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link CommandHandler} for handling {@link PaymentCommand}s for the scenario
 * of paying the amount of a batch rental operation and reserving the corresponding films.
 */
public class PaymentCommandHandler implements CommandHandler<PaymentCommand> {

    private final PricingIntegrationService pricingIntegrationService;

    private final BonusPointsService bonusPointsService;

    private final RentalRepository rentalRepository;

    private final CustomerRepository customerRepository;

    private final FilmRepository filmRepository;

    private final ValidationUtil validationUtil;

    public PaymentCommandHandler(final PricingIntegrationService pricingIntegrationService,
                                 final BonusPointsService bonusPointsService,
                                 final RentalRepository rentalRepository,
                                 final CustomerRepository customerRepository,
                                 final FilmRepository filmRepository,
                                 final ValidationUtil validationUtil) {

        this.pricingIntegrationService = pricingIntegrationService;
        this.bonusPointsService = bonusPointsService;
        this.rentalRepository = rentalRepository;
        this.customerRepository = customerRepository;
        this.filmRepository = filmRepository;
        this.validationUtil = validationUtil;
    }


    /**
     * This handler is responsible to proceed to the payment of the amount of the cost of a batch rental operation
     * and reserve the films that are related to it.
     * @param paymentCommand an instance of {@link PaymentCommand} that contains all the needed info for payoff of
     *                the rental operation and the films that are related to it.
     * @return return the batch id of the batch rental operation.
     * @throws PaymentFailureException in case the pricing gateway integration request fails.
     * @throws CustomerNotFoundException in case the customer id of the customer that concerns the operation is
     *                                   not found.
     */
    @Override
    @Transactional
    public String apply(final PaymentCommand paymentCommand) throws
            PaymentFailureException, CustomerNotFoundException {

        validationUtil.validate(paymentCommand);

        this.proceedWithPayment(paymentCommand);

        final Collection<Long> rentalFilmIds = this.updateRentalsStatusAndGetRelatedFilmIds(paymentCommand);

        this.assignTransactionBonusPointsToCustomer(paymentCommand.getCustomerId(), rentalFilmIds);

        return paymentCommand.getBatchId();
    }

    // ---

    private void proceedWithPayment(final PaymentCommand paymentCommand) throws PaymentFailureException {

        final PricingResponse pricingResponse = pricingIntegrationService.pay(paymentCommand.getCardNumber(),
                                                                              paymentCommand.getPaymentMethod(),
                                                                              paymentCommand.getAmount());
        // if payment fails throw exception
        if (pricingResponse.getRequestStatus() != HttpStatus.OK.value()) {
            throw new PaymentFailureException(String.format(
                    "Payment for customer with id %s failed", paymentCommand.getCustomerId()));
        }
    }

    private Collection<Long> updateRentalsStatusAndGetRelatedFilmIds(final PaymentCommand paymentCommand) {

        final Collection<Long> rentalFilmIds = Lists.newArrayList();
        final List<Rental> updatedRentals = rentalRepository.findAllByBatchId(paymentCommand.getBatchId())
                .stream()
                .peek(rental -> rentalFilmIds.add(rental.getFilmId()))
                .map(rental -> new Rental(rental.getId(),
                        rental.getBatchId(),
                        rental.getFilmId(),
                        rental.getCustomerId(),
                        RentalStatus.RESERVED,
                        rental.getRentalDate(),
                        rental.getDeclaredReturnedDate()))
                .collect(Collectors.toList());

        rentalRepository.save(updatedRentals);

        return rentalFilmIds;
    }

    private void assignTransactionBonusPointsToCustomer(final long customerId, final Collection<Long> rentalFilmIds)
            throws CustomerNotFoundException {

        final Collection<Category> transactionFilmCategories = filmRepository.findAllCategoriesByIdIn(rentalFilmIds)
                .stream()
                .map(FilmCategoryProjection::getCategory)
                .collect(Collectors.toList());

        final Customer updatedCustomer = customerRepository.findById(customerId).map(customer ->
                   new Customer(customer.getId(),
                                customer.getFirstName(),
                                customer.getLastName(),
                                customer.getBonusPoints() + bonusPointsService.calculate(transactionFilmCategories)))
                .orElseThrow(() -> new CustomerNotFoundException(
                       String.format("Customer with id %s not found.", customerId)));

        customerRepository.save(updatedCustomer);
    }
}
