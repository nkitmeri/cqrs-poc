package nick.kitmeridis.videorentalcqrspoc.commandside.handler;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.RentCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.customer.Customer;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Film;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.Rental;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalFilmIdProjection;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalStatus;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalValidationGroups;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.customer.CustomerRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.film.FilmRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental.RentalRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BatchIdGeneratorService;
import nick.kitmeridis.videorentalcqrspoc.exception.CustomerNotFoundException;
import nick.kitmeridis.videorentalcqrspoc.exception.FilmNotAvailableException;
import nick.kitmeridis.videorentalcqrspoc.infrastructure.ValidationUtil;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A {@link CommandHandler} for handling {@link RentCommand}s for the scenario
 * of allocating one or more films for.
 */
public class RentCommandHandler implements CommandHandler<RentCommand> {

    private BatchIdGeneratorService batchIdGeneratorService;

    private CustomerRepository customerRepository;

    private FilmRepository filmRepository;

    private RentalRepository rentalRepository;

    private ValidationUtil validationUtil;

    public RentCommandHandler(final BatchIdGeneratorService batchIdGeneratorService,
                              final CustomerRepository customerRepository,
                              final FilmRepository filmRepository,
                              final RentalRepository rentalRepository,
                              final ValidationUtil validationUtil) {

        this.batchIdGeneratorService = batchIdGeneratorService;
        this.customerRepository = customerRepository;
        this.filmRepository = filmRepository;
        this.rentalRepository = rentalRepository;
        this.validationUtil = validationUtil;
    }

    /**
     * This handler is responsible to create Rental records of status {@link RentalStatus#ALLOCATED}
     * for the customer of the provided, through the input {@link RentCommand}, customer id.
     * Each of the created rental records is related to one film that corresponds to one of the
     * provided, through the input {@link RentCommand}, film ids.
     * @param rentCommand the rent film command that contains information for a (multi) rental operation,
     *                        to be placed.
     * @return thee uuid of the rental(s) operation.
     * @throws CustomerNotFoundException in case customer with the provided customer id not found.
     * @throws FilmNotAvailableException in case one or more of the requested films is not currently available.
     *                                   The exception message contains all the unavailable film names.
     */
    @Override
    @Transactional
    public String apply(final RentCommand rentCommand) throws
            CustomerNotFoundException, FilmNotAvailableException {

        validationUtil.validate(rentCommand);

        final Customer customer = customerRepository.findById(rentCommand.getCustomerId())
                .orElseThrow(() -> new CustomerNotFoundException(
                        String.format("Customer with id %s not found.", rentCommand.getCustomerId())));;

        final Map<Long, Long> rentedIdsCountMap = rentalRepository
                .findAllByFilmIdInAndStatusIn(rentCommand.getFilmIds(),
                        ImmutableSet.of(RentalStatus.ALLOCATED, RentalStatus.RESERVED))
                .stream()
                .map(RentalFilmIdProjection::getFilmId)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        final Set<Film> films = filmRepository.findByIdIn(rentCommand.getFilmIds());

        this.handlePossibleUnavailability(rentedIdsCountMap, films);

        return this.proceedToAllocation(rentCommand, customer, films);
    }

    // ---

    private void handlePossibleUnavailability(final Map<Long, Long> rentedIdsCountMap, final Set<Film> films)
            throws FilmNotAvailableException {

        final Set<String> unavailableFilmNames = films.stream()
                .filter(film -> this.isNotAvailable(film, rentedIdsCountMap))
                .map(Film::getName)
                .collect(Collectors.toCollection(Sets::newTreeSet)); // TreeSet to ensure ordering in the message for
                                                                     // easy testing.

        if (!unavailableFilmNames.isEmpty()) {
            throw new FilmNotAvailableException(String.format(
                    "The following films are not currently available: %s", unavailableFilmNames));
        }
    }

    private boolean isNotAvailable(final Film film, final Map<Long, Long> rentedIdsCountMap) {

        return film.getInitialQuantity() - rentedIdsCountMap.getOrDefault(film.getId(), 0L) == 0L;
    }

    private String proceedToAllocation(final RentCommand rentCommand,
                                       final Customer customer,
                                       final Set<Film> films) {

        final String uuid = batchIdGeneratorService.generate();

        final Collection<Rental> rentals = films
                .stream()
                .map(film -> this.getValidAllocatedRental(rentCommand, customer, uuid, film))
                .collect(Collectors.toList());

        rentalRepository.save(rentals);

        return uuid;
    }

    private Rental getValidAllocatedRental(final RentCommand rentCommand,
                                           final Customer customer,
                                           final String uuid,
                                           final Film film) {

        final Rental rental = new Rental(
                uuid,
                film.getId(),
                customer.getId(),
                RentalStatus.ALLOCATED,
                LocalDate.now(),
                rentCommand.getReturnDate());

        validationUtil.validate(rental, RentalValidationGroups.Create.class);

        return rental;
    }
}
