package nick.kitmeridis.videorentalcqrspoc.commandside;

import nick.kitmeridis.videorentalcqrspoc.exception.ServiceException;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public interface CommandHandler<T extends Command> {

    @NotNull
    String apply(@NotNull final T command) throws ServiceException;
}
