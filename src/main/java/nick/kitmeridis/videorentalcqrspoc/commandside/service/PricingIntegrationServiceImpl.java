package nick.kitmeridis.videorentalcqrspoc.commandside.service;

import nick.kitmeridis.videorentalcqrspoc.commandside.service.dto.PricingResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;

@Validated
public class PricingIntegrationServiceImpl implements PricingIntegrationService {

    private final String pricingGatewayUrl;

    private final RestOperations restOperations;

    public PricingIntegrationServiceImpl(final String pricingGatewayUrl, final RestOperations restOperations) {

        this.pricingGatewayUrl = pricingGatewayUrl;
        this.restOperations = restOperations;
    }

    @Override
    public PricingResponse pay(final String cardNumber, final String paymentMethod, final double amount) {

        final URI fullUrl = UriComponentsBuilder.fromHttpUrl(pricingGatewayUrl)
                .queryParam("cardNumber", cardNumber)
                .queryParam("paymentMethod", paymentMethod)
                .queryParam("amount", Double.toString(amount)).build().toUri();

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        final ResponseEntity<PricingResponse> response = this.restOperations.exchange(
                fullUrl,
                HttpMethod.GET,
                new HttpEntity<>(null, headers),
                PricingResponse.class);

        return response.getBody();
    }
}
