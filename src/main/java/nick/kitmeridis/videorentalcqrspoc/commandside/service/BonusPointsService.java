package nick.kitmeridis.videorentalcqrspoc.commandside.service;

import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Category;

import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * The implementation of this interface implement strategies regarding the bonus point
 * system related to the customers rewards.
 *
 */
public interface BonusPointsService {

    /**
     * Accepts a list of {@link Category film categories} and calculates the total of the bonus points
     * that are extracted form this categories considering the applied strategy.
     * @param categories the categories according to the total of bonus points will be calculated.
     * @return the total sum of the calculated bonus points.
     */
    long calculate(@NotNull final Collection<Category> categories);
}
