package nick.kitmeridis.videorentalcqrspoc.commandside.service;

import org.springframework.validation.annotation.Validated;

import java.util.UUID;

@Validated
public class BatchIdGeneratorServiceImpl implements BatchIdGeneratorService {

    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
