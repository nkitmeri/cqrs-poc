package nick.kitmeridis.videorentalcqrspoc.commandside.service;

import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Category;
import org.springframework.validation.annotation.Validated;

import java.util.Collection;
import java.util.Map;

@Validated
public class BonusPointsServiceImpl implements BonusPointsService {

    private Map<String, Long> bonusPointsMap;

    public BonusPointsServiceImpl(final Map<String, Long> bonusPointsMap) {

        this.bonusPointsMap = bonusPointsMap;
    }

    @Override
    public long calculate(final Collection<Category> categories) {

        return categories.stream()
                .map(category -> {

                    if (Category.NEW.equals(category)) {
                        return bonusPointsMap.getOrDefault("new", 0L);
                    } else if (Category.REGULAR.equals(category)) {
                        return bonusPointsMap.getOrDefault("regular", 0L);
                    } else if (Category.OLD.equals(category)) {
                        return bonusPointsMap.getOrDefault("old", 0L);
                    } else {
                        return 0L;
                    }
                })
                .reduce(0L, (x, y) -> x + y);
    }
}
