package nick.kitmeridis.videorentalcqrspoc.commandside.service.dto;

import lombok.Getter;
import lombok.Value;

@Value
@Getter
public class PricingResponse {

    private int requestStatus;

    private String requestMessage;
}
