package nick.kitmeridis.videorentalcqrspoc.commandside.service;

import nick.kitmeridis.videorentalcqrspoc.commandside.service.dto.PricingResponse;

import javax.validation.constraints.NotNull;

/**
 * The implementations of this interface should act as a
 * <a href="https://martinfowler.com/eaaCatalog/gateway.html">Gateway</a> and integrate with external
 * services that handle the payment system.
 */
public interface PricingIntegrationService {

    @NotNull
    PricingResponse pay(@NotNull final String cardNumber,
                        @NotNull final String paymentMethod,
                        @NotNull final double amount);
}
