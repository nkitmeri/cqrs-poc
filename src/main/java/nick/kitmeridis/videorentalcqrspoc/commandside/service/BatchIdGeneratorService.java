package nick.kitmeridis.videorentalcqrspoc.commandside.service;

import javax.validation.constraints.NotNull;

public interface BatchIdGeneratorService {

    /**
     * Generates a uuid and returns it as a String.
     * @return the generate uuid as a String.
     */
    @NotNull String generate();
}
