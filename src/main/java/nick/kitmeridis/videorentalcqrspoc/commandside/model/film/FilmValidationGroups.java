package nick.kitmeridis.videorentalcqrspoc.commandside.model.film;

public interface FilmValidationGroups {

    interface Create {}
    interface Read {}
    interface Update {}
    interface Delete {}
}
