package nick.kitmeridis.videorentalcqrspoc.commandside.model.rental;

public interface RentalValidationGroups {

    interface Create {}
    interface Read {}
    interface Update {}
    interface Delete {}
}
