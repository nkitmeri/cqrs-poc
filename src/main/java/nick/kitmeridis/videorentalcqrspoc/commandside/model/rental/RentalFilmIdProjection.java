package nick.kitmeridis.videorentalcqrspoc.commandside.model.rental;

import org.springframework.beans.factory.annotation.Value;

import java.util.Collection;

/**
 * A projection interface used as return type in read queries so as to make them more
 * lightweight were applicable.
 * @see nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental.RentalRepository#findAllByFilmIdInAndStatusIn(Collection, Collection)
 */
public interface RentalFilmIdProjection {

    @Value("#{target.filmId}")
    Long getFilmId();
}
