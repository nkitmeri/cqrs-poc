package nick.kitmeridis.videorentalcqrspoc.commandside.model.payment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.FilmValidationGroups;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
@Table(name = "payment")
@NoArgsConstructor
@Getter
public class Payment {

    @Id
    @Column(name = "payment_id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Null(groups = FilmValidationGroups.Create.class)
    @NotNull(groups = {
            PaymentValidationGroups.Read.class,
            PaymentValidationGroups.Update.class,
            PaymentValidationGroups.Delete.class
    })
    private Long id;

    @NotNull
    @Column(name = "rental_batch_id")
    private Long rentalBatchId;

    private double amount;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PaymentStatus status;
}
