package nick.kitmeridis.videorentalcqrspoc.commandside.model.film;

import org.springframework.beans.factory.annotation.Value;

import java.util.Collection;

/**
 * A projection interface used as return type in read queries so as to make them more
 * lightweight were applicable.
 * @see nick.kitmeridis.videorentalcqrspoc.commandside.repository.film.FilmRepository#findAllCategoriesByIdIn(Collection)
 */
public interface FilmCategoryProjection {

    @Value("#{target.category}")
    Category getCategory();
}
