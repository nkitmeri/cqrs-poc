package nick.kitmeridis.videorentalcqrspoc.commandside.model.rental;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.LocalDate;

@Entity
@Table(name = "rental")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Rental {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String batchId;

    @NotNull
    private Long filmId;

    @NotNull
    private Long customerId;

    @Enumerated(EnumType.STRING)
    private RentalStatus status;

    @NotNull
    private LocalDate rentalDate;

    @NotNull
    private LocalDate declaredReturnedDate;

    @Null(groups = RentalValidationGroups.Create.class)
    @NotNull(groups = {
            RentalValidationGroups.Read.class,
            RentalValidationGroups.Update.class,
            RentalValidationGroups.Delete.class
    })
    private LocalDate actualReturnedDate;

    /**
     * Constructor used in create operation (allocate)
     */
    public Rental(@NotNull final String batchId,
                  @NotNull final Long filmId,
                  @NotNull final Long customerId,
                  @NotNull final RentalStatus status,
                  @NotNull final LocalDate rentalDate,
                  @NotNull final LocalDate declaredReturnedDate) {

            this.batchId = batchId;
            this.filmId = filmId;
            this.customerId = customerId;
            this.status = status;
            this.rentalDate = rentalDate;
            this.declaredReturnedDate = declaredReturnedDate;
    }

    /**
     * Constructor used in update operation (reserve)
     */
    public Rental(@NotNull final Long id,
                  @NotNull final String batchId,
                  @NotNull final Long filmId,
                  @NotNull final Long customerId,
                  @NotNull final RentalStatus status,
                  @NotNull final LocalDate rentalDate,
                  @NotNull final LocalDate declaredReturnedDate) {

        this.id = id;
        this.batchId = batchId;
        this.filmId = filmId;
        this.customerId = customerId;
        this.status = status;
        this.rentalDate = rentalDate;
        this.declaredReturnedDate = declaredReturnedDate;
    }
}
