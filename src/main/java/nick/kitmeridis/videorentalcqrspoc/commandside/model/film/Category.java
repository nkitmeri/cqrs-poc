package nick.kitmeridis.videorentalcqrspoc.commandside.model.film;

public enum Category {

    OLD,
    REGULAR,
    NEW
}
