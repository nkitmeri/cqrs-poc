package nick.kitmeridis.videorentalcqrspoc.commandside.model.customer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
@Table(name = "customer")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Customer {

    @Id
    @Column(name = "customer_id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Null(groups = CustomerValidationGroups.Create.class)
    @NotNull(groups = {
            CustomerValidationGroups.Read.class,
            CustomerValidationGroups.Update.class,
            CustomerValidationGroups.Delete.class
    })
    private Long id;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @Min(0L)
    private long bonusPoints;
}
