package nick.kitmeridis.videorentalcqrspoc.commandside.model.payment;

public class PaymentValidationGroups {

    interface Create {}
    interface Read {}
    interface Update {}
    interface Delete {}
}
