package nick.kitmeridis.videorentalcqrspoc.commandside.model.payment;

public enum PaymentStatus {

    DONE, PENDING
}
