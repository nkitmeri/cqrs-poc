package nick.kitmeridis.videorentalcqrspoc.commandside.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

@Converter(autoApply = true)
public class LocalDatePersistenceConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(final LocalDate entityValue) {
        return Optional.ofNullable(entityValue)
                .map(Date::valueOf)
                .orElse(null);
    }

    @Override
    public LocalDate convertToEntityAttribute(final Date databaseValue) {
        return Optional.ofNullable(databaseValue)
                .map(Date::toLocalDate)
                .orElse(null);
    }
}
