package nick.kitmeridis.videorentalcqrspoc.commandside.model.rental;

public enum RentalStatus {

    ALLOCATED,
    RESERVED,
    RESOLVED
}
