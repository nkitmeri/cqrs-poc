package nick.kitmeridis.videorentalcqrspoc.commandside.model.film;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
@Table(name = "film")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Film {

    @Id
    @Column(name = "film_id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Null(groups = FilmValidationGroups.Create.class)
    @NotNull(groups = {
            FilmValidationGroups.Read.class,
            FilmValidationGroups.Update.class,
            FilmValidationGroups.Delete.class
    })
    private Long id;

    @NotEmpty
    private String name;

    @Min(0L)
    @Column(name = "quantity")
    private long initialQuantity;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Category category;
}
