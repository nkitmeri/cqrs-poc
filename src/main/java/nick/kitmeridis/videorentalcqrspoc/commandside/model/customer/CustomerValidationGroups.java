package nick.kitmeridis.videorentalcqrspoc.commandside.model.customer;

public class CustomerValidationGroups {

    interface Create {}
    interface Read {}
    interface Update {}
    interface Delete {}
}
