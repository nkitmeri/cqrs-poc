package nick.kitmeridis.videorentalcqrspoc.commandside.command;

import lombok.Value;
import nick.kitmeridis.videorentalcqrspoc.commandside.Command;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Value
public class ReturnRentalCommand implements Command {

    @NotNull
    final String batchId;

    @NotEmpty
    private String paymentMethod;

    @NotEmpty
    private String cardNumber;

    @Min(0)
    final double extraAmount;
}
