package nick.kitmeridis.videorentalcqrspoc.commandside.command;

import lombok.Value;
import nick.kitmeridis.videorentalcqrspoc.commandside.Command;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Value
public class PaymentCommand implements Command {

    @NotNull
    private Long customerId;

    @NotEmpty
    private String batchId;

    @NotEmpty
    private String paymentMethod;

    @NotEmpty
    private String cardNumber;

    @NotNull
    @Min(0)
    private double amount;
}
