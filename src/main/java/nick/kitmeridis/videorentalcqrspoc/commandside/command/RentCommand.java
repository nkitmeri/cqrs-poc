package nick.kitmeridis.videorentalcqrspoc.commandside.command;

import lombok.Value;
import nick.kitmeridis.videorentalcqrspoc.commandside.Command;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collection;

@Value
public class RentCommand implements Command {

    @NotNull
    private Long customerId;

    @NotNull
    @NotEmpty
    private Collection<Long> filmIds;

    @NotNull
    private LocalDate returnDate;
}
