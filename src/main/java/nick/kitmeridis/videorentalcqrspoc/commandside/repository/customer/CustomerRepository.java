package nick.kitmeridis.videorentalcqrspoc.commandside.repository.customer;

import nick.kitmeridis.videorentalcqrspoc.commandside.model.customer.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {

    Optional<Customer> findById(final Long id);
}
