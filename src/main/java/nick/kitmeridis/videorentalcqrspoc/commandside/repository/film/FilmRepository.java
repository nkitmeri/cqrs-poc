package nick.kitmeridis.videorentalcqrspoc.commandside.repository.film;

import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Film;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.FilmCategoryProjection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface FilmRepository extends PagingAndSortingRepository<Film, Long> {

    Set<Film> findByIdIn(final Collection<Long> filmIds);

    List<FilmCategoryProjection> findAllCategoriesByIdIn(final Collection<Long> filmIds);
}
