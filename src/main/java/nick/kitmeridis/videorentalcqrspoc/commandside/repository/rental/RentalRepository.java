package nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental;

import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.Rental;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalFilmIdProjection;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalStatus;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface RentalRepository extends PagingAndSortingRepository<Rental, Long> {

    Set<Rental> findAllByBatchId(final String batchId);

    List<RentalFilmIdProjection> findAllByFilmIdInAndStatusIn(final Collection<Long>filmId,
                                                              final Collection<RentalStatus> statuses);
}
