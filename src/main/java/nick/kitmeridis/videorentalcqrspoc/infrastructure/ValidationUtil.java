package nick.kitmeridis.videorentalcqrspoc.infrastructure;

import org.springframework.validation.annotation.Validated;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Validated
public class ValidationUtil {

    private Validator validator;

    public ValidationUtil(@NotNull final Validator validator) {
        this.validator = validator;
    }

    public void validate(@NotNull final Object model, final Class... groups) {

        final Set<ConstraintViolation<Object>> violations = this.validator.validate(model, groups);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
