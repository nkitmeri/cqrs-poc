INSERT INTO `film` (`name`,`quantity`,`category`)
VALUES ("The Seven Samurai", 7, "OLD"),
       ("Bonnie and Clyde", 5, "NEW"),
       ("Reservoir Dogs", 4, "OLD"),
       ("Airplane!,", 9, "NEW"),
       ("Pan's Labyrinth", 2, "OLD"),
       ("Doctor Zhivago", 8, "REGULAR"),
       ("The Deer Hunter", 7, "NEW"),
       ("Up", 4, "REGULAR"),
       ("Rocky", 2, "NEW"),
       ("Memento", 3, "REGULAR");

INSERT INTO `customer` (`first_name`,`last_name`,`bonus_points`)
VALUES ("Alexander", "Velazquez", 326),
       ("Wayne", "Poole", 648),
       ("Callum", "Moore", 225),
       ("Timon", "Brooks", 258),
       ("Lance", "Galloway", 217),
       ("Connor", "Perry", 374),
       ("Curran", "Hodges", 246),
       ("Steven", "Joyce", 676),
       ("Gavin", "French", 342),
       ("Luke", "Davidson", 874);

INSERT INTO `rental` (`batch_id`,`film_id`,`customer_id`,`status`,`rental_date`,`declared_returned_date`,`actual_returned_date`)
VALUES ("e04574ce-f477-11e7-8c3f-9a214cf093ae", 2, 3, "RESOLVED", "2017-01-07", "2017-01-08", "2017-01-08"),
       ("e0457906-f477-11e7-8c3f-9a214cf011ae", 1, 8, "RESOLVED", "2017-08-12", "2017-08-21", "2017-08-22"),
       ("e0457b9a-f477-11e7-8c3f-9a214cf022ae", 7, 4, "RESOLVED", "2017-03-25", "2017-03-26", "2017-03-27"),
       ("e0457dac-f477-11e7-8c3f-9a214cf033ae", 7, 10, "RESOLVED", "2017-11-25", "2017-11-26", "2017-11-26"),
       ("e0458432-f477-11e7-8c3f-9a214cf044ae", 4, 5, "RESERVED", "2017-01-24", "2018-01-02", NULL),
       ("e04586a8-f477-11e7-8c3f-9a214cf055ae", 2, 6, "RESOLVED", "2017-07-01", "2017-07-17", "2017-07-17"),
       ("e0458950-f477-11e7-8c3f-9a214cf066ae", 9, 10, "RESOLVED","2017-02-28", "2017-03-03", "2017-03-07"),
       ("e0458b9e-f477-11e7-8c3f-9a214cf077ae", 7, 5, "RESOLVED", "2017-11-09", "2017-11-11", "2017-11-12"),
       ("e0458ebe-f477-11e7-8c3f-9a214cf088ae", 4, 2, "ALLOCATED", "2017-07-29", "2018-01-03", NULL),
       ("e04590ee-f477-11e7-8c3f-9a214cf099ae", 9, 7, "RESOLVED", "2017-10-21", "2017-10-25", "2017-10-25");

INSERT INTO `charging` (`category`,`charging_days`,`base_price`)
VALUES ("OLD", 5, 30),
       ("REGULAR", 3, 30),
       ("NEW", 1, 40);

