CREATE TABLE film (
  film_id BIGINT AUTO_INCREMENT NOT NULL,
  name VARCHAR(100) NOT NULL,
  quantity BIGINT NOT NULL,
  category VARCHAR(100) NOT NULL,
  PRIMARY KEY(film_id)
);

CREATE TABLE customer (
  customer_id BIGINT AUTO_INCREMENT NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  bonus_points BIGINT NOT NULL,
  PRIMARY KEY(customer_id)
);

CREATE TABLE rental (
  id BIGINT AUTO_INCREMENT NOT NULL,
  batch_id VARCHAR(100) NOT NULL,
  film_id BIGINT NOT NULL,
  customer_id BIGINT NOT NULL,
  status VARCHAR(100) NOT NULL,
  rental_date DATE NOT NULL,
  declared_returned_date DATE NOT NULL,
  actual_returned_date DATE NULL DEFAULT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (film_id) REFERENCES film(film_id),
  FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

CREATE TABLE charging (
  category VARCHAR(100) NOT NULL,
  charging_days BIGINT NOT NULL,
  base_price BIGINT NOT NULL,
  PRIMARY KEY(category)
);

CREATE VIEW film_inventory_item AS
  SELECT f.film_id AS film_id,
         f.name AS name,
         c.charging_days AS charging_days,
         c.base_price AS base_price,
         f.quantity - (SELECT COUNT(r.id) FROM rental r WHERE r.film_id = f.film_id) AS availability
  FROM film f
    JOIN charging c ON f.category = c.category
  ORDER BY f.film_id ASC;

CREATE VIEW rental_view_item AS
  SELECT r.batch_id AS batch_id,
         f.name AS film_name,
         r.rental_date AS rental_date,
         r.declared_returned_date AS declared_returned_date,
         (SELECT c.base_price * DATEDIFF(r.declared_returned_date, r.rental_date) / c.charging_days
          FROM charging c
          WHERE c.category = f.category) AS price
  FROM rental r
    JOIN film f ON f.film_id = r.film_id
  ORDER BY r.id DESC;