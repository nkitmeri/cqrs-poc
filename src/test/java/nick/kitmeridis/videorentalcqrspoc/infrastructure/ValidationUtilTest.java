package nick.kitmeridis.videorentalcqrspoc.infrastructure;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import nick.kitmeridis.videorentalcqrspoc.configuration.ValidationConfiguration;
import org.hibernate.validator.constraints.NotBlank;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        ValidationUtilTest.TestConfiguration.class,
        ValidationConfiguration.class
})
public class ValidationUtilTest {

    @Configuration
    public static class TestConfiguration {

        @Bean
        public ValidationUtil validationUtil(final Validator validator) {

            return new ValidationUtil(validator);
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor
    private class TestModel {

        @NotNull
        @Null(groups = TestGroups.Create.class)
        private Long id;

        @NotBlank
        @Size(max = 3, groups = TestGroups.Create.class)
        private String name;

        @Min(5)
        @Max(value = 100, groups = TestGroups.Create.class)
        private long size;
    }

    private interface TestGroups {
        interface Create {}
    }

    @Autowired
    private ValidationUtil validationUtil;

    // ---------------------
    // validate
    // ---------------------

    @Test(expected = ConstraintViolationException.class)
    public void testValidate_whenTheProvidedModelIsNull_throwsConstraintViolationException() {

        validationUtil.validate(null);
    }

    @Test(expected = ConstraintViolationException.class)
    public void testValidate_withoutGroupsWhenGivenModelHasInvalidValues_throwsConstraintViolationException() {

        final TestModel testModel = new TestModel();

        try {
            validationUtil.validate(testModel);
            fail("Should have thrown exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof ConstraintViolationException);
            assertEquals(3, ((ConstraintViolationException) e).getConstraintViolations().size());
            assertTrue(((ConstraintViolationException) e).getConstraintViolations().stream().anyMatch(ex->
                    "id".equals(ex.getPropertyPath().toString()) && "may not be null".equals(ex.getMessage())));
            assertTrue(((ConstraintViolationException) e).getConstraintViolations().stream().anyMatch(ex->
                    "name".equals(ex.getPropertyPath().toString()) && "may not be empty".equals(ex.getMessage())));
            assertTrue(((ConstraintViolationException) e).getConstraintViolations().stream().anyMatch(ex->
                    "size".equals(ex.getPropertyPath().toString()) &&
                            "must be greater than or equal to 5".equals(ex.getMessage())));

            throw e;
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void testValidate_withGroupsWhenGivenModelHasInvalidValues_throwsConstraintViolationException() {

        final TestModel testModel = new TestModel(1L, "four", 101);

        try {
            validationUtil.validate(testModel, TestGroups.Create.class);
            fail("Should have thrown exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof ConstraintViolationException);
            assertEquals(3, ((ConstraintViolationException) e).getConstraintViolations().size());
            assertTrue(((ConstraintViolationException) e).getConstraintViolations().stream().anyMatch(ex->
                    "id".equals(ex.getPropertyPath().toString()) && "must be null".equals(ex.getMessage())));
            assertTrue(((ConstraintViolationException) e).getConstraintViolations().stream().anyMatch(ex->
                    "name".equals(ex.getPropertyPath().toString()) &&
                            "size must be between 0 and 3".equals(ex.getMessage())));
            assertTrue(((ConstraintViolationException) e).getConstraintViolations().stream().anyMatch(ex->
                    "size".equals(ex.getPropertyPath().toString()) &&
                            "must be less than or equal to 100".equals(ex.getMessage())));

            throw e;
        }
    }
}