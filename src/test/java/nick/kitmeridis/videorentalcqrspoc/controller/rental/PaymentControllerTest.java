package nick.kitmeridis.videorentalcqrspoc.controller.rental;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import nick.kitmeridis.videorentalcqrspoc.AbstractIntegrationTest;
import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.PaymentDto;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertEquals;

@FlywayTest
public class PaymentControllerTest extends AbstractIntegrationTest {

    @Rule
    public WireMockRule authorizationWireMockRule = new WireMockRule(9999);

    @Before
    public void reset() {
        authorizationWireMockRule.resetScenarios();
        authorizationWireMockRule.resetMappings();
    }

    @Test(expected = HttpClientErrorException.class)
    @DatabaseSetup(value = "listing_dataset2.xml")
    @ExpectedDatabase(value = "listing_dataset2.xml",
                      assertionMode= DatabaseAssertionMode.NON_STRICT)
    public void testPay_whenPaymentServiceReturnsNotOK_transactionFails() {

        final PaymentDto paymentDto = new PaymentDto(
                2L,
                "63869ef6-f489-11e7-8c3f-9a214cf093ae",
                "Credit_Card",
                "0000111122223333",
                40.0);

        stubFor(get(urlPathEqualTo("/payment"))
                .withQueryParam("cardNumber", equalTo(paymentDto.getCardNumber()))
                .withQueryParam("paymentMethod", equalTo(paymentDto.getPaymentMethod()))
                .withQueryParam("amount", equalTo(Double.toString(paymentDto.getAmount())))
                .willReturn(
                        aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"requestStatus\":\"400\", \"requestMessage\":\"BAD_REQUEST\"}")));

        final HttpEntity<PaymentDto> request = new HttpEntity<>(paymentDto, new HttpHeaders());

        try {
            restOperations.exchange("http://localhost:" + port + "/payment", HttpMethod.POST, request, Void.class);
        } finally {
            verify(getRequestedFor(urlEqualTo("/payment?cardNumber=0000111122223333&paymentMethod=Credit_Card&amount=40.0")));
        }
    }

    @Test
    @DatabaseSetup(value = "listing_dataset2.xml")
    @ExpectedDatabase(value = "listing_dataset_after_payment.xml",
            assertionMode= DatabaseAssertionMode.NON_STRICT)
    public void testPay_whenPaymentServiceReturnsOKAndNoOtherExceptionsAreThrown_transactionSucceeds() {

        final PaymentDto paymentDto = new PaymentDto(
                2L,
                "63869ef6-f489-11e7-8c3f-9a214cf093ae",
                "Credit_Card",
                "0000111122223333",
                40.0);

        stubFor(get(urlPathEqualTo("/payment"))
                .withQueryParam("cardNumber", equalTo(paymentDto.getCardNumber()))
                .withQueryParam("paymentMethod", equalTo(paymentDto.getPaymentMethod()))
                .withQueryParam("amount", equalTo(Double.toString(paymentDto.getAmount())))
                .willReturn(
                        aResponse()
                                .withHeader("Content-Type", "application/json")
                                .withBody("{\"requestStatus\":\"200\", \"requestMessage\":\"SUCCESS\"}")));

        final HttpEntity<PaymentDto> request = new HttpEntity<>(paymentDto, new HttpHeaders());

        final ResponseEntity<String> result = restOperations
                .exchange("http://localhost:" + port + "/payment", HttpMethod.POST, request, String.class);

        assertEquals("63869ef6-f489-11e7-8c3f-9a214cf093ae", result.getBody());

        verify(getRequestedFor(urlEqualTo("/payment?cardNumber=0000111122223333&paymentMethod=Credit_Card&amount=40.0")));
    }

}