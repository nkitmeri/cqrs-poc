package nick.kitmeridis.videorentalcqrspoc.controller.rental;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.google.common.collect.ImmutableList;
import nick.kitmeridis.videorentalcqrspoc.AbstractIntegrationTest;
import nick.kitmeridis.videorentalcqrspoc.controller.rental.dto.RentalDto;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@FlywayTest
public class RentalControllerTest extends AbstractIntegrationTest {

    // -----------------------
    // rent
    // -----------------------

    @Test
    @DatabaseSetup(value = "listing_dataset.xml")
    @ExpectedDatabase(value = "listing_dataset_after_rental.xml",
                      assertionMode= DatabaseAssertionMode.NON_STRICT)
    public void testRent_happyPath_createsRentals() {

        final RentalDto rentalDto = new RentalDto();
        rentalDto.setCustomerId(2L);
        rentalDto.setFilmIds(ImmutableList.of(10L, 20L));
        rentalDto.setReturnDate(LocalDate.of(2018, 6, 6));

        final HttpEntity<RentalDto> request = new HttpEntity<>(rentalDto, new HttpHeaders());
        final ResponseEntity<String> response = restOperations.exchange(
                "http://localhost:" + port + "/rental", HttpMethod.POST, request, String.class);

        final String batchId = response.getBody();

        assertNotNull(batchId);
        assertEquals(5, batchId.split("-").length);
    }
}