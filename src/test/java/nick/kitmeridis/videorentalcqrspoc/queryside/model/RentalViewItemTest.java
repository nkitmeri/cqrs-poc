package nick.kitmeridis.videorentalcqrspoc.queryside.model;

import nick.kitmeridis.videorentalcqrspoc.configuration.ValidationConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        ValidationConfiguration.class
})
public class RentalViewItemTest {

    private RentalViewItem rentalViewItem;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() throws
            ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, IllegalAccessException, InstantiationException {

        final Class clazz = Class.forName("nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalViewItem");
        final Constructor<RentalViewItem> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        this.rentalViewItem = constructor.newInstance();
    }

    // --------------------------
    // getPrice
    // --------------------------
    
    @Test
    public void testGetBasePrice_whenDiffBetweenRentalAnDeclaredDaysIsLessThanBaseChargingDays_returnsBasePrice() {

        ReflectionTestUtils.setField(
                this.rentalViewItem, "rentalDate", LocalDate.of(2018, 1, 9));
        ReflectionTestUtils.setField(
                this.rentalViewItem, "declaredReturnedDate", LocalDate.of(2018, 1, 11));
        ReflectionTestUtils.setField(this.rentalViewItem, "baseChargingDays", 4L);
        ReflectionTestUtils.setField(this.rentalViewItem, "basePrice", 100L);

        assertEquals(100.0, this.rentalViewItem.getPrice(), 0.0);
    }

    @Test
    public void testGetBasePrice_whenDiffBetweenRentalAnDeclaredDaysIsEqualToBaseChargingDays_returnsBasePrice() {

        ReflectionTestUtils.setField(
                this.rentalViewItem, "rentalDate", LocalDate.of(2018, 1, 9));
        ReflectionTestUtils.setField(
                this.rentalViewItem, "declaredReturnedDate", LocalDate.of(2018, 1, 11));
        ReflectionTestUtils.setField(this.rentalViewItem, "baseChargingDays", 3L);
        ReflectionTestUtils.setField(this.rentalViewItem, "basePrice", 100L);

        assertEquals(100.0, this.rentalViewItem.getPrice(), 0.0);
    }

    @Test
    public void testGetBasePrice_whenDiffBetweenRentalAnDeclaredDaysIsMoreThanBaseChargingDays_returnsCorrectPrice() {

        ReflectionTestUtils.setField(
                this.rentalViewItem, "rentalDate", LocalDate.of(2018, 1, 9));
        ReflectionTestUtils.setField(
                this.rentalViewItem, "declaredReturnedDate", LocalDate.of(2018, 1, 15));
        ReflectionTestUtils.setField(this.rentalViewItem, "baseChargingDays", 3L);
        ReflectionTestUtils.setField(this.rentalViewItem, "basePrice", 100L);

        assertEquals(500.0, this.rentalViewItem.getPrice(), 0.0);
    }
}