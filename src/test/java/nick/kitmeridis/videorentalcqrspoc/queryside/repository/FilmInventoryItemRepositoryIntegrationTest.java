package nick.kitmeridis.videorentalcqrspoc.queryside.repository;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import nick.kitmeridis.videorentalcqrspoc.AbstractIntegrationTest;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.FilmInventoryItem;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

import static org.junit.Assert.assertEquals;

@FlywayTest
public class FilmInventoryItemRepositoryIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private FilmInventoryItemRepository filmInventoryItemRepository;

    @Test
    @DatabaseSetup(value = "film_inventory_dataset.xml")
    public void testFindAll_whenInputIsValid_returnsCorrectPage() {

        final Page<FilmInventoryItem> result = filmInventoryItemRepository.findAll(new PageRequest(0, 2));
        final List<FilmInventoryItem> pageItems = result.getContent();

        assertEquals(10L, result.getTotalElements());
        assertEquals(2L, result.getSize());
        assertEquals(5, result.getTotalPages());

        assertEquals(1L, pageItems.get(0).getFilmId());
        assertEquals("The Seven Samurai", pageItems.get(0).getName());
        assertEquals(5L, pageItems.get(0).getBaseChargingDays());
        assertEquals(30L, pageItems.get(0).getBaseChargingPrice());
        assertEquals(2, pageItems.get(0).getAvailableCopies());

        assertEquals(2L, pageItems.get(1).getFilmId());
        assertEquals("Bonnie and Clyde", pageItems.get(1).getName());
        assertEquals(1L, pageItems.get(1).getBaseChargingDays());
        assertEquals(40L, pageItems.get(1).getBaseChargingPrice());
        assertEquals(2, pageItems.get(1).getAvailableCopies());
    }
}
