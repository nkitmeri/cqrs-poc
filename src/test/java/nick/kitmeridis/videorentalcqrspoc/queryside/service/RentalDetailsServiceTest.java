package nick.kitmeridis.videorentalcqrspoc.queryside.service;

import com.google.common.collect.ImmutableList;
import nick.kitmeridis.videorentalcqrspoc.configuration.ValidationConfiguration;
import nick.kitmeridis.videorentalcqrspoc.exception.RentalNotFoundException;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalDetails;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalViewItem;
import nick.kitmeridis.videorentalcqrspoc.queryside.repository.RentalViewItemRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.validation.ConstraintViolationException;
import java.lang.reflect.Constructor;
import java.time.LocalDate;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        RentalDetailsServiceTest.TestConfiguration.class,
        ValidationConfiguration.class
})
public class RentalDetailsServiceTest {

    @Configuration
    public static class TestConfiguration {

        @Bean
        public RentalDetailsService rentalDetailsServiceTest(final RentalViewItemRepository rentalViewItemRepository) {
            return new RentalDetailsServiceImpl(rentalViewItemRepository);
        }

        @Bean
        public RentalViewItemRepository rentalViewItemRepository() {
            return mock(RentalViewItemRepository.class);
        }
    }

    @Autowired
    private RentalDetailsService rentalDetailsService;

    @Autowired
    private RentalViewItemRepository rentalViewItemRepository;

    @Before
    public void setUp() {
        reset(rentalViewItemRepository);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(rentalViewItemRepository);
    }

    // -------------------------------
    // getRentalDetails
    // -------------------------------

    @Test(expected = ConstraintViolationException.class)
    public void testGetRentalDetails_whenTheProvidedBatchIdIsNull_throwsConstraintViolationException() throws Exception {

        rentalDetailsService.getRentalDetails(null);
    }

    @Test(expected = RentalNotFoundException.class)
    public void testGetRentalDetails_whenNoRentalsFoundForTheProvidedId_throwsRentalNotFoundException() throws Exception {

        final String inputBatchId = "one-random-uuid-12345";

        when(rentalViewItemRepository.findAllByBatchId(anyString())).thenReturn(Collections.emptyList());

        try {
            rentalDetailsService.getRentalDetails(inputBatchId);
            fail("Should have thrown exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof RentalNotFoundException);
            assertEquals("No rental items found for batch id: one-random-uuid-12345", e.getMessage());

            throw e;
        } finally {

            verify(rentalViewItemRepository).findAllByBatchId(inputBatchId);
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetRentalDetails_whenRentalsExistForTheProvidedId_aggregatesAndReturnsCorrectRentalDetailsInstance() throws Exception {

        final Class clazz = Class.forName("nick.kitmeridis.videorentalcqrspoc.queryside.model.RentalViewItem");
        final Constructor<RentalViewItem> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);

        final RentalViewItem rentalViewItem1 = constructor.newInstance();
        ReflectionTestUtils.setField(rentalViewItem1, "id", "one-random-uuid-12345");
        ReflectionTestUtils.setField(rentalViewItem1, "batchId", "operation-random-uuid-5555");
        ReflectionTestUtils.setField(rentalViewItem1, "filmName", "12 Monkeys");
        ReflectionTestUtils.setField(
                rentalViewItem1, "rentalDate", LocalDate.of(2018, 1, 20));
        ReflectionTestUtils.setField(
                rentalViewItem1, "declaredReturnedDate", LocalDate.of(2018, 1, 30));
        ReflectionTestUtils.setField(rentalViewItem1, "baseChargingDays", 5L);
        ReflectionTestUtils.setField(rentalViewItem1, "basePrice", 30L);

        final RentalViewItem rentalViewItem2 = constructor.newInstance();
        ReflectionTestUtils.setField(rentalViewItem2, "id", "one-random-uuid-6789");
        ReflectionTestUtils.setField(rentalViewItem2, "batchId", "operation-random-uuid-5555");
        ReflectionTestUtils.setField(rentalViewItem2, "filmName", "Spirited Away");
        ReflectionTestUtils.setField(
                rentalViewItem2, "rentalDate", LocalDate.of(2018, 1, 20));
        ReflectionTestUtils.setField(
                rentalViewItem2, "declaredReturnedDate", LocalDate.of(2018, 1, 30));
        ReflectionTestUtils.setField(rentalViewItem2, "baseChargingDays", 1L);
        ReflectionTestUtils.setField(rentalViewItem2, "basePrice", 40L);


        when(rentalViewItemRepository.findAllByBatchId(anyString())).thenReturn(ImmutableList.of(rentalViewItem1,
                                                                                                 rentalViewItem2));

        final RentalDetails result = rentalDetailsService.getRentalDetails("operation-random-uuid-5555");

        assertEquals("operation-random-uuid-5555", result.getBatchId());
        assertTrue(result.getRentedFilms().stream().anyMatch(rentedFilm ->
                "12 Monkeys".equals(rentedFilm.getFilmName()) && 210.0 == rentedFilm.getFilmPrice()));
        assertTrue(result.getRentedFilms().stream().anyMatch(rentedFilm ->
                "Spirited Away".equals(rentedFilm.getFilmName()) && 440.0 == rentedFilm.getFilmPrice()));
        assertEquals(LocalDate.of(2018, 1, 30), result.getReturnedDay());
        assertEquals(650.0, result.getTotalCost(), 0.0);

        verify(rentalViewItemRepository).findAllByBatchId("operation-random-uuid-5555");
        constructor.setAccessible(false);
    }
}