package nick.kitmeridis.videorentalcqrspoc.queryside.service;

import com.google.common.collect.ImmutableList;
import nick.kitmeridis.videorentalcqrspoc.configuration.ValidationConfiguration;
import nick.kitmeridis.videorentalcqrspoc.queryside.model.FilmInventoryItem;
import nick.kitmeridis.videorentalcqrspoc.queryside.repository.FilmInventoryItemRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        FilmInventoryServiceTest.TestConfiguration.class,
        ValidationConfiguration.class
})
public class FilmInventoryServiceTest {

    @Configuration
    public static class TestConfiguration {

        @Bean
        public FilmInventoryService filmInventoryService(final FilmInventoryItemRepository filmInventoryItemRepository) {
            return new FilmInventoryServiceImpl(filmInventoryItemRepository);
        }

        @Bean
        public FilmInventoryItemRepository filmInventoryItemRepository() {
            return mock(FilmInventoryItemRepository.class);
        }
    }

    @Autowired
    private FilmInventoryService filmInventoryService;

    @Autowired
    private FilmInventoryItemRepository filmInventoryItemRepository;

    @Before
    public void setUp() {
        reset(filmInventoryItemRepository);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(filmInventoryItemRepository);
    }

    // ---------------------------------
    // getFilmInventory
    // ---------------------------------

    @Test(expected = ConstraintViolationException.class)
    public void testGetFilmInventory_whenTheProvidedPageableIsNull_throwsConstraintViolationException() {

        filmInventoryService.getFilmInventory(null);
    }

    @Test
    public void testGetFilmInventory_whenTheProvidedPageableIsValid_makesRepositoryCallAndReturnsPageResult() {

        final Pageable pageable = new PageRequest(0, 10);
        final Page<FilmInventoryItem> expectedResult = new PageImpl<>(ImmutableList.of(
                new FilmInventoryItem()));

        when(filmInventoryItemRepository.findAll(any(Pageable.class))).thenReturn(expectedResult);

        assertEquals(expectedResult, filmInventoryService.getFilmInventory(pageable));

        verify(filmInventoryItemRepository).findAll(pageable);
    }

}