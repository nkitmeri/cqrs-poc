package nick.kitmeridis.videorentalcqrspoc.commandside.handler;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import nick.kitmeridis.videorentalcqrspoc.commandside.Command;
import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.RentCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.customer.Customer;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Category;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Film;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.Rental;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalFilmIdProjection;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalStatus;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.customer.CustomerRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.film.FilmRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental.RentalRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BatchIdGeneratorService;
import nick.kitmeridis.videorentalcqrspoc.configuration.ValidationConfiguration;
import nick.kitmeridis.videorentalcqrspoc.exception.CustomerNotFoundException;
import nick.kitmeridis.videorentalcqrspoc.exception.FilmNotAvailableException;
import nick.kitmeridis.videorentalcqrspoc.infrastructure.ValidationUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        RentCommandHandlerTest.TestConfiguration.class,
        ValidationConfiguration.class
})
@SuppressWarnings("unchecked")
public class RentCommandHandlerTest {

    @Configuration
    public static class TestConfiguration {

        @Bean
        public CommandHandler commandHandler(final BatchIdGeneratorService batchIdGeneratorService,
                                             final CustomerRepository customerRepository,
                                             final FilmRepository filmRepository,
                                             final RentalRepository rentalRepository,
                                             final ValidationUtil validationUtil) {

            return new RentCommandHandler(
                    batchIdGeneratorService,
                    customerRepository,
                    filmRepository,
                    rentalRepository,
                    validationUtil);
        }

        @Bean
        public BatchIdGeneratorService batchIdGeneratorService() {
            return mock(BatchIdGeneratorService.class);
        }

        @Bean
        public CustomerRepository customerRepository() {
            return mock(CustomerRepository.class);
        }

        @Bean
        public FilmRepository filmRepository() {
            return mock(FilmRepository.class);
        }

        @Bean
        public RentalRepository rentalRepository() {
            return mock(RentalRepository.class);
        }
    }

    @Autowired
    private CommandHandler commandHandler;

    @Autowired
    private BatchIdGeneratorService batchIdGeneratorService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RentalRepository rentalRepository;

    @Before
    public void setUp() {
        reset(batchIdGeneratorService,
              customerRepository,
              filmRepository,
              rentalRepository);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(
                batchIdGeneratorService,
                customerRepository,
                filmRepository,
                rentalRepository);
    }

    // --------------
    // apply
    // --------------

    @Test(expected = ConstraintViolationException.class)
    public void testApply_whenTheProvidedCommandIsNull_throwsConstraintViolationException() throws Exception {

        commandHandler.apply(null);
    }

    @Test(expected = ConstraintViolationException.class)
    public void testApply_whenInputCommandIsInvalid_throwsConstraintViolationException() throws Exception {

        final Command command = new RentCommand(null, Collections.emptyList(), null);

        try {
            commandHandler.apply(command);
            fail("Should have thrown Exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof ConstraintViolationException);
            assertEquals(3, ((ConstraintViolationException) e).getConstraintViolations().size());

            throw e;
        }
    }

    @Test(expected = CustomerNotFoundException.class)
    public void testApply_whenNoCustomerFoundForTheProvidedCustomerId_throwsCustomerNotFoundException() throws Exception {

        final Command command = new RentCommand(
                1L,
                ImmutableList.of(10L, 20L, 30L),
                LocalDate.of(2018, 1, 30));

        when(customerRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        try {
            commandHandler.apply(command);
            fail("Should have thrown Exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof CustomerNotFoundException);
            assertEquals("Customer with id 1 not found.", e.getMessage());
            assertEquals(404, ((CustomerNotFoundException) e).getStatusCode());

            throw e;
        } finally {
            verify(customerRepository).findById(1L);
        }
    }

    @Test(expected = FilmNotAvailableException.class)
    public void testApply_whenInputCommandContainsNotAvailableFilms_throwsFilmNotAvailableException() throws Exception {

        final Customer customer = new Customer(1L, "John", "Snow", 100L);

        final Film film1 = new Film(10L ,"It", 3, Category.NEW);
        final Film film2 = new Film(20L ,"Pi", 1, Category.OLD);

        final Command command = new RentCommand(
                1L,
                ImmutableList.of(10L, 20L),
                LocalDate.of(2018, 1, 30));

        final RentalFilmIdProjection rentalFilmIdProjection1 = mock(RentalFilmIdProjection.class);
        final RentalFilmIdProjection rentalFilmIdProjection2 = mock(RentalFilmIdProjection.class);
        final RentalFilmIdProjection rentalFilmIdProjection3 = mock(RentalFilmIdProjection.class);
        final RentalFilmIdProjection rentalFilmIdProjection4 = mock(RentalFilmIdProjection.class);

        when(customerRepository.findById(any(Long.class))).thenReturn(Optional.of(customer));
        when(rentalFilmIdProjection1.getFilmId()).thenReturn(10L);
        when(rentalFilmIdProjection2.getFilmId()).thenReturn(10L);
        when(rentalFilmIdProjection3.getFilmId()).thenReturn(10L);
        when(rentalFilmIdProjection4.getFilmId()).thenReturn(20L);
        when(rentalRepository
                .findAllByFilmIdInAndStatusIn(anyCollectionOf(Long.class), anyCollectionOf(RentalStatus.class)))
                .thenReturn(ImmutableList.of(
                        rentalFilmIdProjection1,
                        rentalFilmIdProjection2,
                        rentalFilmIdProjection3,
                        rentalFilmIdProjection4));
        when(filmRepository.findByIdIn(anyCollectionOf(Long.class))).thenReturn(ImmutableSet.of(film1, film2));

        try {
            commandHandler.apply(command);
            fail("Should have thrown exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof FilmNotAvailableException);
            assertEquals("The following films are not currently available: [It, Pi]", e.getMessage());

            throw e;
        } finally {

            verify(customerRepository).findById(1L);
            verify(rentalRepository).findAllByFilmIdInAndStatusIn(
                    ImmutableList.of(10L, 20L),
                    ImmutableSet.of(RentalStatus.ALLOCATED, RentalStatus.RESERVED));
            verify(filmRepository).findByIdIn(ImmutableList.of(10L, 20L));
        }
    }

    @Test
    public void testApply_whenInputCommandIsValid_makesCorrectCallsAndReturnsRentalBatchId() throws Exception {

        final Customer customer = new Customer(1L, "John", "Snow", 100L);

        final Film film1 = new Film(10L ,"It", 3, Category.NEW);
        final Film film2 = new Film(20L ,"Pi", 1, Category.OLD);

        final Command command = new RentCommand(
                1L,
                ImmutableList.of(10L, 20L),
                LocalDate.of(2018, 1, 30));

        final String uuid = UUID.randomUUID().toString();

        final RentalFilmIdProjection rentalFilmIdProjection = mock(RentalFilmIdProjection.class);

        when(customerRepository.findById(any(Long.class))).thenReturn(Optional.of(customer));
        when(rentalFilmIdProjection.getFilmId()).thenReturn(10L);
        when(rentalRepository
                .findAllByFilmIdInAndStatusIn(anyCollectionOf(Long.class), anyCollectionOf(RentalStatus.class)))
            .thenReturn(Collections.singletonList(rentalFilmIdProjection));
        when(filmRepository.findByIdIn(anyCollectionOf(Long.class))).thenReturn(ImmutableSet.of(film1, film2));
        when(batchIdGeneratorService.generate()).thenReturn(uuid);
        when(rentalRepository.save(anyCollectionOf(Rental.class))).thenReturn(anyCollectionOf(Rental.class));

        final String batchId = commandHandler.apply(command);

        assertNotNull(batchId);
        assertEquals(uuid, batchId);

        verify(customerRepository).findById(1L);
        verify(rentalRepository).findAllByFilmIdInAndStatusIn(
                        ImmutableList.of(10L, 20L),
                        ImmutableSet.of(RentalStatus.ALLOCATED, RentalStatus.RESERVED));
        verify(filmRepository).findByIdIn(ImmutableList.of(10L, 20L));
        verify(batchIdGeneratorService).generate();
        verify(rentalRepository).save(ImmutableList.of(
                new Rental(batchId,
                        10L,
                        1L,
                        RentalStatus.ALLOCATED,
                        LocalDate.now(),
                        LocalDate.of(2018, 1, 30)),
                new Rental(batchId,
                        20L,
                        1L,
                        RentalStatus.ALLOCATED,
                        LocalDate.now(),
                        LocalDate.of(2018, 1, 30))));
    }
}