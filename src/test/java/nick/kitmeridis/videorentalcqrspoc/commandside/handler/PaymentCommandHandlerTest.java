package nick.kitmeridis.videorentalcqrspoc.commandside.handler;

import nick.kitmeridis.videorentalcqrspoc.commandside.Command;
import nick.kitmeridis.videorentalcqrspoc.commandside.CommandHandler;
import nick.kitmeridis.videorentalcqrspoc.commandside.command.PaymentCommand;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.customer.Customer;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.Category;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.film.FilmCategoryProjection;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.Rental;
import nick.kitmeridis.videorentalcqrspoc.commandside.model.rental.RentalStatus;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.customer.CustomerRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.film.FilmRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.repository.rental.RentalRepository;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.BonusPointsService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.PricingIntegrationService;
import nick.kitmeridis.videorentalcqrspoc.commandside.service.dto.PricingResponse;
import nick.kitmeridis.videorentalcqrspoc.configuration.ValidationConfiguration;
import nick.kitmeridis.videorentalcqrspoc.exception.CustomerNotFoundException;
import nick.kitmeridis.videorentalcqrspoc.exception.PaymentFailureException;
import nick.kitmeridis.videorentalcqrspoc.infrastructure.ValidationUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        PaymentCommandHandlerTest.TestConfiguration.class,
        ValidationConfiguration.class
})
@SuppressWarnings("unchecked")
public class PaymentCommandHandlerTest {

    @Configuration
    public static class TestConfiguration {

        @Bean
        public CommandHandler commandHandler(final PricingIntegrationService pricingIntegrationService,
                                             final BonusPointsService bonusPointsService,
                                             final RentalRepository rentalRepository,
                                             final CustomerRepository customerRepository,
                                             final FilmRepository filmRepository,
                                             final ValidationUtil validationUtil) {

            return new PaymentCommandHandler(pricingIntegrationService,
                                             bonusPointsService,
                                             rentalRepository,
                                             customerRepository,
                                             filmRepository,
                                             validationUtil);
        }

        @Bean
        public PricingIntegrationService pricingIntegrationService() {
            return mock(PricingIntegrationService.class);
        }

        @Bean
        public BonusPointsService bonusPointsService() {
            return mock(BonusPointsService.class);
        }

        @Bean
        public RentalRepository rentalRepository() {
            return mock(RentalRepository.class);
        }

        @Bean
        public CustomerRepository customerRepository() {
            return mock(CustomerRepository.class);
        }

        @Bean
        public FilmRepository filmRepository() {
            return mock(FilmRepository.class);
        }
    }

    @Autowired
    private CommandHandler commandHandler;

    @Autowired
    private PricingIntegrationService pricingIntegrationService;

    @Autowired
    private BonusPointsService bonusPointsService;

    @Autowired
    private RentalRepository rentalRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Before
    public void setUp() {
        reset(pricingIntegrationService,
              bonusPointsService,
              filmRepository,
              rentalRepository,
              customerRepository);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(
                pricingIntegrationService,
                bonusPointsService,
                filmRepository,
                rentalRepository,
                customerRepository);
    }

    // --------------
    // apply
    // --------------

    @Test(expected = ConstraintViolationException.class)
    public void testApply_whenTheProvidedCommandIsNull_throwsConstraintViolationException() throws Exception {

        commandHandler.apply(null);
    }

    @Test(expected = ConstraintViolationException.class)
    public void testApply_whenTheProvidedCommandIsInvalid_throwsConstraintViolationException() throws Exception {

        final Command command = new PaymentCommand(null, "", "", null, -1.0);

        try {
            commandHandler.apply(command);
            fail("Should have thrown Exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof ConstraintViolationException);
            assertEquals(5, ((ConstraintViolationException) e).getConstraintViolations().size());

            throw e;
        }
    }

    @Test(expected = PaymentFailureException.class)
    public void testApply_whenThePricingIntegrationServiceReturnsNotOKStatusCode_throwsPaymentFailureException() throws Exception {

        final Command command = new PaymentCommand(
                1L,
                "one-random-uuid-12345",
                "Paypal",
                "0000111122223333",
                100.0);

        when(pricingIntegrationService.pay(anyString(), anyString(), anyDouble()))
                .thenReturn(new PricingResponse(400, "BAD_REQUEST"));

        try {
            commandHandler.apply(command);
            fail("Should have thrown Exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof PaymentFailureException);
            assertEquals("Payment for customer with id 1 failed", e.getMessage());

            throw e;
        } finally {
            verify(pricingIntegrationService).pay("0000111122223333", "Paypal", 100.0);
        }
    }

    @Test(expected = CustomerNotFoundException.class)
    public void testApply_whenNoCustomerFoundForTheProvidedId_throwsCustomerNotFoundException() throws Exception {

        final Command command = new PaymentCommand(
                1L,
                "one-random-uuid-12345",
                "Paypal",
                "0000111122223333",
                100.0);

        final Rental rental = new Rental("one-random-uuid-12345",
                10L,
                1L,
                RentalStatus.ALLOCATED,
                LocalDate.now(),
                LocalDate.of(2018, 1, 30));

        final FilmCategoryProjection filmCategoryProjection = mock(FilmCategoryProjection.class);

        when(pricingIntegrationService.pay(anyString(), anyString(), anyDouble()))
                .thenReturn(new PricingResponse(200, "OK"));
        when(rentalRepository.findAllByBatchId(anyString())).thenReturn(Collections.singleton(rental));
        when(rentalRepository.save(anyCollectionOf(Rental.class))).thenReturn(Collections.emptyList());
        when(filmRepository.findAllCategoriesByIdIn(anyCollectionOf(Long.class)))
                .thenReturn(Collections.singletonList(filmCategoryProjection));
        when(filmCategoryProjection.getCategory()).thenReturn(Category.NEW);
        when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            commandHandler.apply(command);
            fail("Should have thrown Exception.");
        } catch (final Exception e) {

            assertTrue(e instanceof CustomerNotFoundException);
            assertEquals("Customer with id 1 not found.", e.getMessage());

            throw e;
        } finally {

            final ArgumentCaptor<Collection> rentalArgumentCaptor = ArgumentCaptor.forClass(Collection.class);

            verify(pricingIntegrationService).pay("0000111122223333", "Paypal", 100.0);
            verify(rentalRepository).findAllByBatchId("one-random-uuid-12345");
            verify(rentalRepository).save(rentalArgumentCaptor.capture());
            verify(filmRepository).findAllCategoriesByIdIn(Collections.singletonList(10L));
            verify(customerRepository).findById(1L);

            assertEquals(RentalStatus.RESERVED,
                    ((Rental)rentalArgumentCaptor.getValue().stream().findFirst().get()).getStatus());
        }
    }

    @Test
    public void testApply_whenInputCommandIsValid_makesCorrectCallsAndReturnsRentalBatchId() throws Exception {

        final Command command = new PaymentCommand(
                1L,
                "one-random-uuid-12345",
                "Paypal",
                "0000111122223333",
                100.0);

        final Rental rental = new Rental("one-random-uuid-12345",
                10L,
                1L,
                RentalStatus.ALLOCATED,
                LocalDate.now(),
                LocalDate.of(2018, 1, 30));

        final Customer initialCustomer = new Customer(1L,"Arya","Stark",10L);

        final FilmCategoryProjection filmCategoryProjection = mock(FilmCategoryProjection.class);

        when(pricingIntegrationService.pay(anyString(), anyString(), anyDouble()))
                .thenReturn(new PricingResponse(200, "OK"));
        when(rentalRepository.findAllByBatchId(anyString())).thenReturn(Collections.singleton(rental));
        when(rentalRepository.save(anyCollectionOf(Rental.class))).thenReturn(Collections.emptyList());
        when(filmRepository.findAllCategoriesByIdIn(anyCollectionOf(Long.class)))
                .thenReturn(Collections.singletonList(filmCategoryProjection));
        when(filmCategoryProjection.getCategory()).thenReturn(Category.NEW);
        when(bonusPointsService.calculate(anyCollectionOf(Category.class))).thenReturn(2L);
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(initialCustomer));

        commandHandler.apply(command);

        final ArgumentCaptor<Collection> rentalArgumentCaptor = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Customer> updatedCustomerArgumentCaptor = ArgumentCaptor.forClass(Customer.class);

        verify(pricingIntegrationService).pay("0000111122223333", "Paypal", 100.0);
        verify(rentalRepository).findAllByBatchId("one-random-uuid-12345");
        verify(rentalRepository).save(rentalArgumentCaptor.capture());
        verify(filmRepository).findAllCategoriesByIdIn(Collections.singletonList(10L));
        verify(bonusPointsService).calculate(Collections.singletonList(Category.NEW));
        verify(customerRepository).findById(1L);
        verify(customerRepository).save(updatedCustomerArgumentCaptor.capture());

        assertEquals(RentalStatus.RESERVED,
                ((Rental)rentalArgumentCaptor.getValue().stream().findFirst().get()).getStatus());
        assertEquals(12L, updatedCustomerArgumentCaptor.getValue().getBonusPoints());
    }
}