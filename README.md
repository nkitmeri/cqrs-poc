# Video Rental Test App


## Usage 

The easiest way to deploy and demonstrate the application is by using the mix of Docker and BASH scripts that can be found in the folder named `deployment-scripts`.

After cloning this repository from a Linux machine open a terminal and `cd` in the `deployment-scripts` folder under the project's root.

**Important:** The Linux machine must have installed and running the Docker daemon ([Ubuntu example](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#supported-storage-drivers)).

From there execute the following commands: `chmod +x docker-deploy.sh && ./docker-deploy.sh [true|false]`. 

**Notice** :Insert `true` for running unit/integration test during the application's build process or `false` to skip them. 

It will take some time to download build and run the Docker containers and to resolve the Maven dependencies and deploy the application.

This process is responsible for the following 3 things:

* **MySQL**: It creates a Docker container that runs a MySQL instance that accepts connections from any network with a passwordless root user. (*Notice: this setup is far from secure and should be used only for prototyping/demonstration, never in a production environment*) 

* **[Mock Node.js server](https://github.com/clue/docker-json-server)**: It creates a Docker container running a mock server that accepts HTTP request and returns a predefined response that is seed during the container's build. It is serves as an external payment service for the application.

* **Spring Boot application**: It resolves dependencies and builds the application using a Maven wrapper script and then deploys it in a docker container from an image that is automatically created using the [Spotify Docker Maven Plugin](https://github.com/spotify/docker-maven-plugin).

All 3 containers export their services related ports to associated ports on locahost.

When the aforementioned process finishes, open a web browser on the local machine and 
visit this [Swagger Page](http://localhost:8080/swagger-ui.html) from where you can make requests to the applications API endpoints.

From this page you can execute a full application usage scenario as follows:

1. Expand the Swagger entry under the section with title **Inventory Controller Impl**.
 
    - Make a GET request to get a list of the video store's inventory, which is consisted by entries that have been seeded to database during the application's deployment.

2. Expand the Swagger entry under the section with title **Rental Controller Impl** and corresponds to the `/rental` endpoint. 

    - Click on the example value area. Fill the JSON inside textarea with example values and then make a POST request. You will be provided with a uuid that corresponds to the pending rental operation.

3. Expand the Swagger entry under the section with title **Rental Details Controller Impl** and corresponds to the `/rental-details/{batchId}` endpoint. 

    - Add the `batchId` obtained from the previous request to fill the endpoint's path parameter, in the input area, and make a GET request to see a review of the pending rental process.

4. Expand the Swagger entry under the section with title **Payment Controller Impl** and click on the example value area. 
     
    - Fill the JSON inside textarea with example values (use the uuid from the previous request's response in the JSON's `batchId` field and the customer's id you used in the previous request in the `customerId` field)

5. Expand the Swagger entry under the section with title **Rental Details Controller Impl** and corresponds to the `/rental-details/return/{batchId}` endpoint. 

    - Add the `batchId` obtained from the previous request to fill the endpoint's path parameter, in the input area, and make a GET request to see a review of the  rental operation's state regarding and especially any possible extra charge due to return delay.
   
6. Expand the Swagger entry under the section with title **Rental Controller Impl** and corresponds to the `/rental/return` endpoint. 

    - Click on the example value area. Fill the JSON inside textarea with the associated values from the previous requests and then make a POST request. This request marks the end of a rental process by updating the database making the process' rented films available again.

**Notice**: In every step of this scenario you can connect to the docker container that runs the MySQL instance and verify the results. Open a terminal in the Host OS and execute the following commands:

`mysql -h localhost -P 3306 --protocol=tcp -u root`

`use video_rental` or `video_rental_test` (for the database that is dedicated to the integration test)

`show tables;`

### Using [Postman](https://www.getpostman.com/) instead of Swagger

You can interact with the application's endpoints through Postman.
You just need to import the Swagger's generated JSON.
From the header panel click on **Import** and then on **Import From Link** tab add the following value `http://localhost:8080/v2/api-docs`

## Application Stack And Deployment

#### Spring Boot
The application is implemented with [Spring Boot](http://localhost:8080/swagger-ui.html).

Spring Boot has been chosen because it offers solutions for many aspects of the application.

More specifically, by utilizing a series of its starter projects (see [Sping Initializr](https://start.spring.io/)) this application handles the following matters:

* **Web Server**: Several lightweight web servers can be bundled in the application's fat jar and server it. The choices vary between Tomcat, Jetty and Undertow. This application uses Undertow.

* **Dependency Injection**: By using the capabilities offered by the core Spring Framework, dependency injection has been easily implemented and can be managed with almost no effort in an isolated scope.

* **REST Layer**: REST Controllers with their methods have been implemented with no extra effort and by only using the related annotations for mapping any kind of HTTP request to the suitable path. Also, Spring starter web project offers libraries that have been used for outgoing HTTP calls from the application (this capability has been utilized for the application's service component that calls an external pricing server -explained later-).

* **Database Layer**: Spring boot's useful capabilities have been exploited for both the Database setup and the Repository Layer's implementation.
  
    - Database setup: Spring boot integrates perfectly with [Flyway](https://flywaydb.org/) with almost zero configuration. This application utilizes this integration both for the "production" database setup and for integration testing databases setups.
  
    - Repository Layer: [Spring Data Project](http://projects.spring.io/spring-data/) and more specifically [Spring Data JPA](https://projects.spring.io/spring-data-jpa/) has been used for the implementation of the Repository Layer, which is the application layer that interact with the database.
  
* **Validation**: The application utilizes mechanisms for performing validation both on the Models and on the Service methods. This is capable by using [JSR-303](http://beanvalidation.org/1.0/spec/) specification's implementations (annotation found in the javax package and on hibernate validator project and also the extended annotation offered by Spring Framework).   

* **Testing**: Another useful Spring Boot starter project that has been used, concerns the testing of the application. Spring Boot starter test integrates with Junit and offers Runners and annotations that make the task for implementing unit and integration tests very easy.



#### Beyond Spring Boot

A series of external frameworks and libraries have been used in the application, beyond Spring Boot and its ecosystem.

Those are:

* **[Project Lombok](https://projectlombok.org/)**: The excellent Java Library that is used to reduce boilerplate code by offering capabilities auto generating among others mutators,  accessors, constructors, builders and default ovverrides of equals method, hashcode method and toString method, in the compile time.

* **[Google Guava Library](https://github.com/google/guava)**: The well known 3rd party library has been used as it offers excellent Collection extensions. Immutable Collections and factory methods for initializing them are used everywhere in the application as until Java 8 (which is the version that this application has been implemented) no similar capabilities are offered by the core JDK.
 
* **[Orika Mapper](https://github.com/orika-mapper/orika)**: This excellent Bean Mapping framework has been configured and used from the mapping from/to incoming/outgoing DTOs to/from domain models, view models and commands.

* **[Swagger](https://swagger.io/)**: The Java library ot the, so called, "world's most popular API tooling" has been used to annotate both the application's Controllers and DTOs, for documentation purposes, but also for the on the fly build of a web client that can been accessed through an HTML page and used to make requests to the application endpoints.

* **Several libraries for testing**: [DBUnit](http://dbunit.sourceforge.net/), [WireMock](http://wiremock.org/), [Flyway test extension](https://github.com/flyway/flyway-test-extensions/wiki/Usage-flyway-spring-test) and [Spring test DBUnit extensions](https://springtestdbunit.github.io) dependencies have been used for the database integration tests and for the mocking of the application's interactions with external APIs. 

* **[Docker](https://www.docker.com/)**: The popular containerization platform has been utilized to make the deployment of application feasible by, *literally*, running just one command.

* **[BASH](https://www.gnu.org/software/bash/)**: Minimal BASH scripts have been written for the deployment and orchestration of the Docker containers that compose the application's deployment environment.

## Implementation

This section is a discussion about the application's implementation.
It quickly describes the design of the application and in some points serves as complement of the Javadoc documentation that can be found in the source.

#### Setup project instructions 

To open the source code of the Project just import it in your IDE. For example in Intellij go to `File->Open..` then navigate to the cloned directory and
select the pom.xml on the next popup select select `Open as project`.

**Important**: Make sure that you hav installed the **Lombok plugin** for your IDE.
For Intellij:

* Go to `File > Settings > Plugins`
* Click on `Browse repositories...`
* Search for Lombok Plugin
* Click on `Install plugin`

Then go to `Settings > Build, Execution, Deployment > Compiler > Annotation Processors` and click on `Enable annotation proccessing` box.

Please visit [Lombok's home page](https://projectlombok.org/) for setup instructions for other IDEs.

#### CQRS

A prototype of the [Command Query Responsibility Segregation (CQRS)](http://microservices.io/patterns/data/cqrs.html) has been utilized for the implementation of the application.

This means that while not every detail of the pattern has been  considered (for example there is no dedicated report database neither the pattern is extended with event sourcing) the separation of the responsibilities of the read and write sides should be easily perceived both in the source code and in the database as they are isolated from each other by using different packaging and different types of tables.

More specifically, all the write side components are under a subpackage named `commanside` in the source code of the project, while all the read side components are under a subpackage named `queryside`. 

Regarding the database, the cqrs has been simulated by using real Tables for the command side area and Views for the read side.

#### MVC(SR)

Regardless the prototyping of the CQRS Pattern, the application is further designed using a classic monolithic Model-View-Controller-Service-Repository Architecture.

**Model**: View models form the read side and Commands and Domain/Persistence (there is no distinction between them in contrast of DDD paradigm) models of the write side, compose this layer.

**View**: As this application implements a REST API, DTOs play the role of the view layer.

**Controller**: Spring Boot REST Controllers compose this layer. They accept requests and perform the mapping to/and from DTOs after delegating the actual processing to the Service layer.

**Service**: This layer is composed by the services of the read side and the Handlers with their complementary services in the write side.

**Repository**: Spring Data JPA Repositories have been implemented and associated to each of the Domain/Persistence Models, in the default spring data way that results more likely in a blend of the [Repository](https://martinfowler.com/eaaCatalog/repository.html) and [DAO](https://en.wikipedia.org/wiki/Data_access_object) patterns, rather than an pure implementation of the first.

### Technical Details

##### Validation

As already have been discussed there are 2 points where validation is applied using to implementations of the [JSR-303](http://beanvalidation.org/1.0/spec/) specification.

* The first logical unit where validation is performed is in the Model layer of the write side. Both Commands that are mapped from the incoming DTOs as well as Domain/Persistence Models that are to be saved are validated using the implementation of the `ValidationUtil` class and its injected `javax.validation.Validator`, that can be found under the subpackage named `infrustructure` in the project's source code.

* The other logical unit of validation is composed by the methods of the Service Layer. For this reason *input parameters* and *return types* of Service methods signatures are annotated with JSR-303 compatible annotations and implementation classes are annotated with the Spring's `@Validated` annotation. The application context perform a validation on each method call and return ,using an instance of the `org.springframework.validation.beanvalidation.MethodValidationPostProcessor` which is configured on the `ValidationConfiguration` class, under the package `configuration` and some of the Spring's magic.

**Notice**: Due to time limitations, the described validation processes are not found in every aspect of the project, but indicative usage should be easily found by navugating in the source code.

##### Mapper

Spring Boot automatically maps the incoming request and out going responses that have JSON body in the declared DTOs exactly the time they come/go in the Controller Layer.

This is the only layer that DTOs reach inside the application.
The associated values are mapped in model lasses and passed down for further processing.

As already have been discussed, for this purpose, the excellent Orika mapping library is used.

The instantiation and configuration code of the necessary mapper's instance can be found in the class `MapperConfiguration` under the package `configuration`.

The specific DTO - Model mapping configuration can be found in the `dto` subpackages of the controller layer packages inside the classes named `*MapperConfigurer`.

##### Spring Configuration

Under the package `configuration` further project configuration can be found.

* `BeanConfiguration`: It contains the instantiation of the projects beans that are used to be injected as dependencies where needed.
* `SystemConfiguration`: It contains the instantiation of the `Datasource` bean that used for the connection to the database and the `RestTemplate` bean that is used to perform request to the mock external payment service through the `PricingIntegrationService` gateway.
* `SpringFoxConfiguration` contains the default instantiation that Swagger its Spring Boot integration needs so as to configure the client.

Also, several configuration related information can be found in the `application.yml` file under the projects `resources` directory, in the form of key-value pairs.

Finally, the same directory contains the sql scripts that are used by Flyway to create the database schema and seed the tables with demo data, during the application's start up.

## Testing

[Junit 4](http://junit.org/junit4/) has been used to test the application components.

#### Unit testing

The strategy that have been followed for unit testing is [Tests with Mock Objects](https://martinfowler.com/articles/mocksArentStubs.html).
For the purpose of Mocking object the [Mockito](http://site.mockito.org/) framework has been used alongside with Junit.

The verification process of its unit test is thorough, meaning that after the each test execution and after the implicit verifications a general verification is perform to ensure that no unwanted interaction happen during the tested methods execution (see the `@After` methods in the unit test source classes).

#### Integration Testing

The integration tests run inside the application's context. This is possible by the annotations, dependencies and configuration code that can be found inside `AbstractIntegrationTest` class, which all integration test classes extend.

What happens there, is loading the configurations for instantiated application's beans, setting up the connection to the test database and creating the schema using flyway scripts and seeding it with DBUnit data sets. 

The integration tests can be further divided into 3 categories:

* **End to end tests without external interactions**: As the  name implies those tests perform an execution flow by making an HTTP request to the endpoint that corresponds to a Controller's method, and ending up to the database that have been seed with test data, after passing from all the layers that have been described above. After the execution of the flow, assertions are performed to the Response DTOs. Spring `RestTemplate` is used to perform the initial request inside the tests. (See `RentalControllerTest` class)

* **End to end tests with external interactions**: Those are similar to the tests of the above category. The only difference arises from the fact that those test concern the flows that interact with the external payment service, and thus they mock a request to it and specify the potential answer of the external service. To do this, the [WireMock](http://wiremock.org/) framework is being used. (see `PaymentControllerTest` class)

* **Repository tests**: Those test are for the repository layer and check the query methods of the repository interfaces against the test database, in isolation. Like the first category's tests they use Flyway sql scripts and DBUnit data sets for seeding and verifying the database's state after the tested method's execution but they do not perform http calls or any code excecution outside the repository layer. (see `FilmInventoryItemRepositoryIntegrationTest` class)

**Notice**: Due to time limitations, the application's test coverage is below the acceptable values of a real life application. However as described above, the implemented tests cover a large variety of testing needs that can arise from an enterprise application's execution flow. 