#!/usr/bin/env bash

scripts_root=$(cd "$(dirname "$0")" ; pwd -P)

cd ${scripts_root}

docker network create --driver=bridge common-network

bash ./mysql/docker-mysql.sh
bash ./mock-pricing/docker-mock-pricing-server.sh
bash ./app/docker-app.sh