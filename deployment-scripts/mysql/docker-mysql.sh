#!/usr/bin/env bash

cwd=$(cd "$(dirname "$0")" ; pwd -P)

docker build -t mysql6686 ${cwd}

docker run --rm -it --net common-network \
                    -p 3306:3306 \
                    --name mysql6686 \
                    -e MYSQL_ALLOW_EMPTY_PASSWORD=YES \
                    -e MYSQL_ROOT_HOST=% \
                    -d mysql6686

# if the previous command returned error then probably the container already exists.
# So just try to start it
if [ ! $? -eq 0 ]; then
    docker start mysql6686
fi