#!/usr/bin/env bash

cwd=$(cd "$(dirname "$0")" ; pwd -P)

project_root=${cwd}/../../

docker build -t video-rental-test6686 ${cwd}

cd ${project_root} && ./mvnw clean package docker:build -Dmaven.test.skip=$1

docker run --rm -it --net common-network -p 8080:8080 video-rental-test6686

