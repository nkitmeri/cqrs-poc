#!/usr/bin/env bash

seed_file=$(cd "$(dirname "$0")" ; pwd -P)/mock-pricing.js

docker run --rm -it --net common-network \
                    -d -p 3000:80 \
                    --name mock-pricing-server-6686 \
                    -v ${seed_file}:/data/file.js \
                    clue/json-server

# if the previous command returned error then probably the container already exists.
# So just try to start it
if [ ! $? -eq 0 ]; then
    docker start mock-pricing
fi